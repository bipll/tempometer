#ifndef TEMPOMETER__DEFS_H_f064233fc0a396d10e28ea4c38b11e8500e3a3c0
#define TEMPOMETER__DEFS_H_f064233fc0a396d10e28ea4c38b11e8500e3a3c0

#include <limits>
#include <type_traits>

#include <QDateTime>
#include <QGeoPositionInfo>
#include <QObject>

namespace defs {

inline constexpr auto nothing = std::numeric_limits<qreal>::quiet_NaN();

inline constexpr auto nothingl = std::numeric_limits<qint64>::min();

inline constexpr bool defined(qreal x) noexcept { return x == x; }

inline constexpr bool defined(qint64 x) noexcept { return x != nothingl; }

inline constexpr bool defined(auto... values) noexcept { return (defined(values) && ...); }

inline constexpr qreal norm(auto... values) noexcept {
    return std::sqrt(((values * values) + ...));
}

inline constexpr double coordScale = 185000;

inline constexpr qreal tooSlow = 0.25;

inline constexpr qint64 tooSoon = 250;


inline qint64 now() noexcept {
    return QDateTime::currentMSecsSinceEpoch();
}


template<class Value>
inline constexpr auto calm(Value newValue, const Value oldValue = {}, const Value halfCircle = 180) {
    while(newValue > oldValue + halfCircle) newValue -= 2 * halfCircle;
    while(newValue < oldValue - halfCircle) newValue += 2 * halfCircle;
    return newValue;
}


inline constexpr auto mid(auto v1, auto v2) noexcept {
    if(defined(v1)) [[likely]] {
        if(defined(v2)) [[likely]] {
            return (v1 + v2) / 2;
        }
        return v1;
    }
    return v2;
}


void connectAll(auto sender, auto signal, auto slot, auto receiver, auto... receivers) {
    if constexpr(std::is_pointer_v<decltype(receiver)>) {
        QObject::connect(sender, signal, receiver, slot);
        if constexpr(sizeof...(receivers)) connectAll(sender, signal, slot, receivers...);
    }
    else connectAll(sender, signal, receiver, receivers...);
}

}

#endif
