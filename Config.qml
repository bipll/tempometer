import QtQuick 2.15
import QtQuick.Controls 2.15
import 'Format.js' as Fmt
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import 'Units.js' as SI

Item {
    id: config
    property var currentSettings: db.currentSettings()

    property real mile: currentSettings['mile']
    onMileChanged: Fmt.mile = mile
    property real yard: currentSettings['yard']
    onYardChanged: Fmt.yard = yard

    property real speedFactor: currentSettings['speedFactor']
    onSpeedFactorChanged: Fmt.speedFactor = speedFactor

    property real wptSensitivity: currentSettings['wptSensitivity']
    property bool addNewWpts: currentSettings['addNewWpts']
    property bool startLapsOnWpts: currentSettings['startLapsOnWpts']

    function yardSuffix(yd) {
        console.log('-')
        console.log('--')
        console.log('---')
        console.log('----')
        console.log("Lookup unit letter for", yd)
        console.log('----')
        console.log('---')
        console.log('--')
        console.log('-')
        let lkp = SI.yardLookup(yd)
        return lkp === undefined? '?' : yardChoice.choices[lkp]
    }

    property string metersSuffix: yardSuffix(yard, currentSettings)
    onMetersSuffixChanged: Fmt.meterSuffix = metersSuffix

    function inflate() {
        theButton.left()

        currentSettings = db.currentSettings()

        console.log('current settings:')
        for(let k in currentSettings) console.log('\t', k, '->', currentSettings[k])
        mileChoice.setIndex(currentSettings.mile)
        yardChoice.setIndex(currentSettings.yard)
        speedChoice.setIndex(currentSettings.speedFactor)
    }

    function save() {
        console.log('Saving settings:')
        for(let k in currentSettings) console.log('\t', k, '->', currentSettings[k])
        theButton.settings()
        db.saveSettings(currentSettings)
    }

    function set(key, value) { currentSettings[key] = value; currentSettings = currentSettings }

    onCurrentSettingsChanged: system.currentIndex = SI.system(currentSettings)

    SettingsBlock {
        id: units
        section: 'Units'
        height: 240
        y: 10
        ComboBox {
            id: system
            x: 10
            y: 30
            model: ['metric', 'imperial', 'nautical']
            Component.onCompleted: currentIndex = SI.system(currentSettings)
            //onActivated: { console.log("SI ACTIVATED", index); mile.currentIndex = index; yard.currentIndex = index; speed.currentIndex = index }
        }

        ConfigChoice {
            id: mileChoice; y:  85; choiceX: 120; lookup: SI.mileLookup; systemIndex: system.currentIndex; label: 'dist'; choices: ['km', 'mi', 'nmi']
            onActivated: config.set('mile',  SI.miles[index])
            onCustom: config.set('mile', value)
        }
        ConfigChoice {
            id: yardChoice; y: 135; choiceX: 120; lookup: SI.yardLookup; systemIndex: system.currentIndex; label: 'short dist'; choices: ['m', 'y', 'm']
            onActivated: config.set('yard',  SI.yards[index])
            onCustom: config.set('yard', value)
        }
        ConfigChoice {
            id: speedChoice; y: 185; choiceX: 120; lookup: SI.speedLookup; systemIndex: system.currentIndex; label: 'speed'; choices: ['km/h', 'mph', 'kt', 'm/s']
            onActivated: config.set('speedFactor',  SI.speeds[index])
            onCustom: config.set('speedFactor', value)
        }
    }

    SettingsBlock {
        id: wpts
        section: 'Waypoints'
        height: 200
        y: 270

        ConfigValue { y: 30; label: 'Wpt radius'; labelOffset: 10
            Column {
                x: 180
                Slider {
                    id: slider; from: 0.3; to: 50; value: wptSensitivity / yard
                    onMoved: config.set('wptSensitivity', value)
                }
                TextInput {
                    width: 90; anchors.horizontalCenter: parent.horizontalCenter; text: Fmt.toString(slider.value)
                    validator: DoubleValidator { bottom: 0.3 / yard; top: 50 / yard }
                    onAccepted: config.set('wptSensitivity', yard * Number.parseFloat(text))
                }
            }
        }

        ConfigValue { y: 110; labelOffset: 5; label: 'Wpts start laps'
            CheckBox { x: 180; checked: currentSettings.startLapsOnWpts
                onToggled: set('startLapsOnWpts', checked)
            }
        }
        ConfigValue { y: 150; labelOffset: 5; label: 'Enroute new wpts'
            CheckBox { x: 180; checked: currentSettings.addNewWpts
                onToggled: set('addNewWpts', checked)
            }
        }
    }
}
