import QtQuick 2.15

SequentialAnimation {
    id: animation
    property var target
    property string property: 'line.color'
    property color from: 'white'
    property color to: 'black'
    property int duration: 1500

    loops: Animation.Infinite

    onTargetChanged: console.log('New target:', target)

    ColorAnimation {
        target: animation.target
        property: animation.property

        from: animation.from
        to: animation.to
        duration: animation.duration
        easing.type: Easing.InOutQuad
    }
    ColorAnimation {
        target: animation.target
        property: animation.property

        from: animation.to
        to: animation.from
        duration: animation.duration
        easing.type: Easing.InOutQuad
    }
}
