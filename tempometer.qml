import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtPositioning 5.12
import QtQml 2.12
import QtLocation 5.15
import QtQuick.LocalStorage 2.15

import 'Format.js' as Fmt
import QtQml.StateMachine 1.15 as SM
import 'Util.js' as U
import 'Sql.js' as Sql

import com.gitlab.bipll.tempometer.batterylevel 1.0

ApplicationWindow {
    id: app

    width: 480
    height: 640
    visible: true
    title: qsTr("Tempometer")

    property int indieWidth
    property int indieHeight
    function flashOn() {
        indieWidth = width
        indieHeight = height
        width *= Screen.devicePixelRatio
        height *= Screen.devicePixelRatio
    }

    function flashOff() {
        width = indieWidth
        heigth = indieHeight
    }


    property real tooSlow: 0.25

    property int batteryUpdatePeriod: 5

    property real avgDelay: 10000

    property var db: new Sql.Db(LocalStorage)

    property real screenDeformation
    Component.onCompleted: {
        screenDeformation = Screen.height / 4 * 3 / Screen.width
        console.log("===================================================================== SCREEN DEFORMATION:", screenDeformation)
        console.log("WxH:", width, height)
        //main.
    }

    Timer {
        id: clockworkMechanism
        property date now: new Date()
        interval: 1000 - now.getMilliseconds()
        repeat: true
        running: true
        onTriggered: {
            now = new Date()
            if(--batteryUpdatePeriod == 0) {
                batteryUpdatePeriod = 5
                batteryLevel.update()
            }
        }
    }

    BatteryLevel {
        id: batteryLevel
    }

    header: ToolBar {
        height: 60
        RowLayout {
            id: toolBar
            anchors.fill: parent
            property int fontSize: 28
            spacing: 10
            Label {
                text: 'Tempometer'
                font.pixelSize: parent.fontSize
            }
            Label {
                id: clock
                text: clockworkMechanism.now.toLocaleTimeString(Qt.locale(), 'hh:mm:ss')
                font.pixelSize: parent.fontSize
                //fontSizeMode: Text.VerticalFit
            }
            BatteryIndicator {
                Layout.preferredWidth: 64
                level: batteryLevel.level
                fontSize: parent.fontSize
            }
            Button {
                id: theButton
                signal wrench
                signal back

                Layout.preferredWidth: 40
                visible: true
                enabled: true
                contentItem: Image {
                    id: theImage
                    anchors.margins: 5
                    //source: 'images/settings.png'
                    fillMode: Image.Pad
                }

                function inflate() {
                    visible = true
                    enabled = true
                    Layout.preferredWidth = 40
                }
                function left() {
                    inflate()
                    theImage.source = 'images/left.png'
                    theButton.clicked.disconnect(wrench)
                    theButton.clicked.connect(back)
                }
                function settings() {
                    inflate()
                    theImage.source = 'images/settings.png'
                    theButton.clicked.disconnect(back)
                    theButton.clicked.connect(wrench)
                }

                function deflate() {
                    Layout.preferredWidth = 0
                    visible = false
                    enabled = false
                }

                //onClicked: save.cancel()

                property int uiIndex
                onWrench: { uiIndex = ui.currentIndex; ui.currentIndex = 3; cfg.inflate(); theButton.back.connect(backFromCfg) }

                function backFromCfg() {
                    cfg.save()
                    ui.currentIndex = uiIndex
                    theButton.back.disconnect(backFromCfg)
                    settings()
                }
            }
        }
    }

    StackLayout {
        id: ui
        anchors.fill: parent
        Main {
            id: main
        }
        SaveDialog {
            id: save
            //plugin: main.plugin
        }
        LibView {
            id: libView
        }
        Config {
            id: cfg
        }
    }
    property bool recording: main.recording
    property bool saved: true
    property TraxDialog trax: libView.trax

    SM.StateMachine {
        id: lifecycle
        initialState: zero
        running: true
        property SM.State preconfigState
        SM.State {
            id: zero
            onEntered: startSession()
            SM.SignalTransition {
                signal: main.lapStopSaveClicked
                targetState: lib
            }
            SM.SignalTransition {
                signal: main.startPauseRecClicked
                targetState: record
            }
        }
        SM.State {
            id: lib
            onEntered: showLib()
            SM.SignalTransition {
                signal: theButton.back
                guard: saved
                targetState: zero
            }
            SM.SignalTransition {
                signal: theButton.back
                guard: !saved
                targetState: unsaved
            }
            SM.SignalTransition {
                signal: trax.review
                targetState: zero
                onTriggered: main.showReview(trax.selectedTrack, trax.laps)
            }
            SM.SignalTransition {
                signal: trax.goOn
                targetState: pause
                onTriggered: main.preload(trax.selectedTrack, trax.laps)
            }
        }
        SM.State {
            id: record
            initialState: start
            SM.State {
                id: start
                onEntered: started()
                SM.SignalTransition {
                    signal: main.startPauseRecClicked
                    targetState: pause
                }
                SM.SignalTransition {
                    signal: main.lapStopSaveClicked
                    targetState: lap
                }
            }
            SM.State {
                id: lap
                onEntered: {
                    //logCoord(pos.position.coordinate, speed)
                    newLap()
                }
                SM.SignalTransition {
                    signal: main.startPauseRecClicked
                    targetState: pause
                }
                SM.SignalTransition {
                    signal: main.lapStopSaveClicked
                    targetState: lap
                }
            }
        }
        SM.State {
            id: pause
            onEntered: paused()
            SM.SignalTransition {
                signal: main.startPauseRecClicked
                targetState: lap
            }
            SM.SignalTransition {
                signal: main.lapStopSaveClicked
                targetState: stop
            }
        }
        SM.State {
            id: stop
            onEntered: stopped()
            SM.SignalTransition {
                signal: main.startPauseRecClicked
                targetState: record
            }
            SM.SignalTransition {
                signal: main.lapStopSaveClicked
                targetState: saveDialog
            }
        }

        SM.State {
            id: saveDialog
            onEntered: saveTrack()
            SM.SignalTransition {
                signal: save.saved
                targetState: zero
                onTriggered: saved = true
            }
            SM.SignalTransition {
                signal: save.canceled
                targetState: zero
                onTriggered: saved = false
            }
            SM.SignalTransition {
                signal: theButton.clicked
                targetState: zero
                onTriggered: saved = false
            }
        }
        SM.State {
            id: unsaved
            onEntered: showUnsaved()
            SM.SignalTransition {
                signal: main.startPauseRecClicked
                targetState: record
            }
            SM.SignalTransition {
                signal: main.lapStopSaveClicked
                targetState: saveDialog
            }
        }
    }

    Component {
        id: curveFactory
        Lap { line.width: 12; line.color: U.defaultColor; backend: MapPolyline.OpenGLLineStrip }
    }

    Component {
        id: polygonFactory
        MapPolygon { border.width: 8; backend: MapPolygon.OpenGL }
    }

    property bool noZero
    function started() {
        main.started()
        save.clear()
        theButton.deflate()
    }

    property real finishTime: 0
    function paused() {
        finishTime = U.now()
        main.paused()
        theButton.settings()
    }

    function stopped() {
        main.stopped()
    }

    function newLap() {
        main.newLap()
        theButton.deflate()
    }

    function saveTrack() {
        theButton.left()
        save.curves = main.curves
        save.inflate()
        main.visible = false
        ui.currentIndex = 1
    }

    function backToMain() {
        theButton.settings()
        main.visible = true
        ui.currentIndex = 0
    }

    function startSession() {
        backToMain()
        main.reset()
    }

    function showUnsaved() {
        backToMain()
        main.unsaved()
    }

    function showLib() {
        console.log('Entering trax, saved =', saved)
        theButton.left()
        trax.inflate()
        ui.currentIndex = 2
    }

    property color flickering
    property bool currentlyHighlighted: false

    Flickering {
        target: app
        property: 'flickering'
        running: currentlyHighlighted
    }
}
