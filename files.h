#ifndef FILES_H
#define FILES_H

#include <QDir>
#include <QStandardPaths>
#include <QString>
#include <QGeoCoordinate>

#include "defs.h"

#include <cmath>

namespace files {

inline const QDir mainFolder{
    []{
        auto paths = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
        switch(paths.size()) {
        [[unlikely]] case 0: return QString{};
        [[unlikely]] case 1: return paths.front();
        default: return paths[1];
        }
    }()
};

inline const QString filenameBase{"current"};

inline QString filename(quint32 lapNo) {
    return filenameBase + '.' + QString::number(lapNo) + ".bin";
}

inline QString filepath(quint32 lapNo, QString subfolder = {}) {
    return subfolder.isEmpty()?
                mainFolder.absoluteFilePath(filename(lapNo)) :
                mainFolder.absoluteFilePath(std::move(subfolder) + '/' + filename(lapNo));
}

inline constexpr std::uint32_t FILE_FORMAT = 0;

template<std::uint32_t = FILE_FORMAT> struct DiskPoint;

#pragma pack(push, 1)
template<> struct [[gnu::packed]] DiskPoint<FILE_FORMAT> {
    static constexpr std::int64_t nan = -(1 << 25);

    std::int64_t lat: 26;
    std::int64_t lon: 26;
    std::uint64_t ele: 14;
    std::uint64_t timestampMs: 62;    // do not record rides before 01.01.1970

    DiskPoint() noexcept = default;

    DiskPoint(qreal lat, qreal lon, qreal ele, qint64 ts) noexcept
        : lat(scaleCoord(lat))
        , lon(scaleCoord(lon))
        , ele(std::isnan(ele)? 0 : std::round((ele + 1000.0)))
        , timestampMs(ts)
    {}

    DiskPoint(QGeoCoordinate const &coord, qint64 ts) noexcept
        : DiskPoint(
            coord.isValid()? coord.latitude() : defs::nothing,
            coord.isValid()? coord.longitude() : defs::nothing,
            coord.isValid()? coord.altitude() : defs::nothing,
            ts
           )
    {}

    constexpr qreal latitude() const noexcept { return lat == nan? defs::nothing : lat / defs::coordScale; }
    constexpr qreal longitude() const noexcept { return lon == nan? defs::nothing : lon / defs::coordScale; }
    constexpr qreal elevation() const noexcept { return ele? ele - 1000.0 : defs::nothing; }
    constexpr qint64 timestamp() const noexcept { return timestampMs; }

    constexpr bool isValid() const noexcept { return timestampMs; } // ... and even on 01.01.1970, please wait one second past midnight before recording

private:
    static std::int64_t scaleCoord(qreal coord) noexcept {
        return std::isnan(coord)? nan : std::round(coord * defs::coordScale);
    }
};
#pragma pack(pop)

static_assert(sizeof(DiskPoint<FILE_FORMAT>) == 16);

}

#endif // FILES_H
