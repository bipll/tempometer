import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: lib
    StackLayout {
        width: parent.width
        anchors.top: parent.top
        anchors.bottom: bar.top

        currentIndex: bar.currentIndex
        TraxDialog { id: traxDialog; onUnhighlighted: main.hideReview() }
        PoiDialog {}
    }
    TabBar {
        id: bar
        width: parent.width
        anchors.bottom: parent.bottom
        position: TabBar.Footer
        TabButton { text: 'Tracks' }
        TabButton { text: 'POIs' }
    }

    property TraxDialog trax: traxDialog
}
