import QtQuick 2.15
import QtPositioning 5.15
import QtLocation 5.15
import QtQuick.Shapes 1.15
import 'Util.js' as U
import 'Format.js' as Fmt

MapQuickItem {
    id: arrow
    property int number: 0
    property var base
    property var goal

    property color color: 'darkgrey'
    property real strokeWidth: 3

    required property Map device

    property real length: 100
    property real azimuth: base.azimuthTo(goal)
    property real direction: azimuth * d2r
    property real wing: Math.PI * 0.875
    property real wingSpan: 10
    property real margin: effectiveLength * 0.2

    property real d2r: Math.PI / 180
    property real sin: -Math.cos(direction)
    property real cos: Math.sin(direction)
    coordinate: base

    property real effectiveLength: howFar(device.zoomLevel)
    property real arrowWidth: cos * effectiveLength
    property real arrowHeight: sin * effectiveLength
    //device.onZoomLevelChanged: effectiveLength = howFar()
    //Component.onCompleted: effectiveLength = Qt.binding(howFar)

    sourceItem: Shape {
        ShapePath {
            id: shape

            fillColor: 'transparent'
            strokeWidth: arrow.strokeWidth
            strokeColor: arrow.color
            startX: margin * cos
            startY: margin * sin
            PathLine { x: arrowWidth; y: arrowHeight }
            PathLine { relativeX: Math.sin(direction + wing) * wingSpan; relativeY: -Math.cos(direction + wing) * wingSpan }
            PathMove { relativeX: -Math.sin(direction + wing) * wingSpan; relativeY: Math.cos(direction + wing) * wingSpan }
            PathLine { relativeX: Math.sin(direction - wing) * wingSpan; relativeY: -Math.cos(direction - wing) * wingSpan }
        }
        Text {
            text: Fmt.roman(number, Fmt.DIGITS)
            color: arrow.color
            style: Text.Outline
            styleColor: U.outlineColor
            property real radius: 4 + Math.max(width, height) * 0.5
            x: shape.startX + arrowWidth * 0.5 + sin * radius - width * 0.5
            y: shape.startY + arrowHeight * 0.5 - cos * radius - height * 0.5
        }
        Text {
            text: Fmt.dist(base.distanceTo(goal))
            color: arrow.color
            style: Text.Outline
            styleColor: U.outlineColor
            property real radius: 4 + Math.max(width, height) * 0.5
            x: shape.startX + arrowWidth * 0.5 - sin * radius - width * 0.5
            y: shape.startY + arrowHeight * 0.5 + cos * radius - height * 0.5
        }
    }

    function howFar(zl) {
        let distantBegin = device.fromCoordinate(base, false)
        let distantEnd = device.fromCoordinate(goal, false)
        let x = distantEnd.x - distantBegin.x
        let y = distantEnd.y - distantBegin.y
        let rsq = x * x + y * y
        if(!(rsq > length * length)) return Math.sqrt(rsq)
        return length
    }
}
