package com.gitlab.bipll.tempometer;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.app.Notification;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import org.qtproject.qt5.android.bindings.QtService;
import android.Manifest.permission;
import android.content.pm.PackageManager;
import android.os.IBinder;

import android.location.Location;
import android.os.Looper;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnSuccessListener;

import java.lang.Double;
import java.lang.Float;
import java.lang.Void;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;

public class TrackWriter extends QtService {
    private static final String TAG = "TEMPOMETER WRITER SERVICE";
    private static int ONGOING_NOTIFICATION_ID = 73;
    public static final String CHANNEL_ID = "TempometerServiceChannel";

    private Notification notification = null;

    private void createNotificationChannel() {
        NotificationChannel serviceChannel = new NotificationChannel(
                CHANNEL_ID,
                "Tempometer Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
                );
        NotificationManager manager = getSystemService(NotificationManager.class);
        manager.createNotificationChannel(serviceChannel);
    }

    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "TEMPOMETER WRITER SERVICE: starting Java service");
        if(instance != null) return START_STICKY;
        Log.d(TAG, "TEMPOMETER WRITER SERVICE: instance wasn't null");
        instance = this;

        Log.d(TAG, "TEMPOMETER WRITER SERVICE: create channel");
        createNotificationChannel();
        Log.d(TAG, "TEMPOMETER WRITER SERVICE: make notification");
        notification = makeNotification(0);

        // Notification ID cannot be 0.
        Log.d(TAG, "TEMPOMETER WRITER SERVICE: start foreground");
        startForeground(ONGOING_NOTIFICATION_ID, notification);
        Log.d(TAG, "TEMPOMETER WRITER SERVICE: return");
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopForeground(STOP_FOREGROUND_REMOVE);
        super.onDestroy();
    }

    public static Notification makeNotification(int status) {
        String str = statusString(status);

        Intent notificationIntent = new Intent(instance, TrackWriter.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(instance, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE);

        return new NotificationCompat.Builder(instance, CHANNEL_ID)
            .setContentTitle("Tempometer")
            .setContentText(str)
            .setSmallIcon(R.drawable.notification)
            .setContentIntent(pendingIntent)
            .setTicker(str)
            .setOngoing(true)
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .build();
    }

    public static void setStatus(int status) {
        if(instance == null) return;
        NotificationManager notificationManager = (NotificationManager)instance.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ONGOING_NOTIFICATION_ID, makeNotification(status));
    }

    private static String statusString(int status) {
        switch(status) {
            case 1: return "Recording";
            case 2: return "Paused";
            default: return "Running";
        }
    }

    private static TrackWriter instance = null;
}
