package com.gitlab.bipll.tempometer;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import java.lang.Double;

public class BatteryListener {
    private static Intent batteryIntent = null;

    public static double level(Context activity) {
        if(activity == null) throw new NullPointerException("Null activity");

        IntentFilter batteryFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        batteryIntent = activity.registerReceiver(null, batteryFilter);
        if(batteryIntent == null) return Double.NaN;

	double level = (double)batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
	double scale = (double)batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        return level >= 0 && scale > 0? level * 100.0 / scale : Double.NaN;
    }
}
