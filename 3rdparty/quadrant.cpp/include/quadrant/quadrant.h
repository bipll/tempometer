#ifndef QUADRANT__QUADRANT_H_5d0bc270d72fefa111631c43d2bb505aaf659dc9
#define QUADRANT__QUADRANT_H_5d0bc270d72fefa111631c43d2bb505aaf659dc9

#include "bits/runtime_sequence.h"
#include "prelude.h"
#include "tuple.h"

#include <algorithm>
#include <climits>
#include <compare>
#include <cstdint>
#include <iterator>
#include <numeric>
#include <type_traits>

namespace nstd {

template<class T> concept has_signum = std::is_arithmetic_v<T>
	|| std::is_same_v<T, std::partial_ordering>
	|| std::is_same_v<T, std::weak_ordering>
	|| std::is_same_v<T, std::strong_ordering>;


struct quadrant {
	using value_type = std::uintmax_t;
	using max_size = std::integral_constant<unsigned char, sizeof(value_type) * CHAR_BIT / 2>;
	static constexpr auto max_size_v = force_v<max_size>;

	template<has_signum... Coords> requires (sizeof...(Coords) <= max_size_v)
	constexpr quadrant(Coords... coords)
	noexcept(noexcept(fold(std::index_sequence_for<Coords...>{}, coords...)))
		: value(fold(std::index_sequence_for<Coords...>{}, coords...))
		, mask(ones(std::index_sequence_for<Coords...>{}))
	{}

	constexpr operator value_type() const noexcept { return value; }

	template<has_signum... Coords> requires (sizeof...(Coords) <= max_size_v)
	constexpr quadrant &assign(Coords... coords)
	noexcept(noexcept(fold(std::index_sequence_for<Coords...>{}, coords...)))
	{
		value = fold(std::index_sequence_for<Coords...>{}, coords...);
		mask = ones(std::index_sequence_for<Coords...>{});
		return *this;
	}

	constexpr std::weak_ordering operator<=>(quadrant const &that) const noexcept {
		return (value & that.mask) <=> (that.value & mask);
	}

	constexpr bool operator==(quadrant const &that) const noexcept {
		return (*this <=> that) == 0;
	}

	constexpr bool setAt(std::size_t i) const noexcept {
		return (value >> i * 2) & 3;
	}

	constexpr int operator[](std::size_t i) const noexcept {
		int rv = (value >> i * 2) & 3;
		return rv? rv - 2 : rv;
	}

	template<has_signum Coord>
	constexpr quadrant &operator=(Coord that)
	noexcept(noexcept(assign(that)))
	{
		return assign(that);
	}

	template<class Left, class Right>
	requires (std::tuple_size_v<std::remove_cvref_t<Left>> <= max_size_v &&
			std::tuple_size_v<std::remove_cvref_t<Left>> == std::tuple_size_v<std::remove_cvref_t<Right>>)
	static constexpr auto cmp(Left &&left, Right &&right)
	noexcept(noexcept(Compare{}(std::forward<Left>(left),
				    std::forward<Right>(right),
				    std::make_index_sequence<std::tuple_size_v<std::remove_cvref_t<Left>>>{})))
	{
		return Compare{}(std::forward<Left>(left),
				 std::forward<Right>(right),
				 std::make_index_sequence<std::tuple_size_v<std::remove_cvref_t<Left>>>{});
	}

	template<class Left, class Right, class Comp>
	requires (std::tuple_size_v<std::remove_cvref_t<Left>> <= max_size_v &&
			std::tuple_size_v<std::remove_cvref_t<Left>> == std::tuple_size_v<std::remove_cvref_t<Right>>)
	static constexpr auto cmp(Left &&left, Right &&right, Comp &&comp)
	noexcept(noexcept(Compare{}(std::forward<Left>(left),
				    std::forward<Right>(right),
				    std::forward<Comp>(comp),
				    std::make_index_sequence<std::tuple_size_v<std::remove_cvref_t<Left>>>{})))
	{
		return Compare{}(std::forward<Left>(left),
				 std::forward<Right>(right),
				 std::forward<Comp>(comp),
				 std::make_index_sequence<std::tuple_size_v<std::remove_cvref_t<Left>>>{});
	}

	template<class TL, class TR>
	static constexpr auto cmp(std::initializer_list<TL> left, std::initializer_list<TR> right)
	noexcept(noexcept(bits::RuntimeSequence::invokeWith<max_size_v>(left.size(), Compare{},
									bits::initializerList<max_size_v>(left),
									bits::initializerList<max_size_v>(right))))
	{
		return bits::RuntimeSequence::invokeWith<max_size_v>(left.size(), Compare{},
								     bits::initializerList<max_size_v>(left),
								     bits::initializerList<max_size_v>(right));
	}

	template<class TL, class TR, class Comp>
	static constexpr auto cmp(std::initializer_list<TL> left, std::initializer_list<TR> right, Comp &&comp)
	noexcept(noexcept(bits::RuntimeSequence::invokeWith<max_size_v>(left.size(), Compare{},
									bits::initializerList<max_size_v>(left),
									bits::initializerList<max_size_v>(right),
									std::forward<Comp>(comp))))
	{
		return bits::RuntimeSequence::invokeWith<max_size_v>(left.size(), Compare{},
								     bits::initializerList<max_size_v>(left),
								     bits::initializerList<max_size_v>(right),
								     std::forward<Comp>(comp));
	}

private:
	template<class Coord>
	static constexpr value_type normalize(Coord coord)
	noexcept(noexcept((coord <=> 0) < 0) && noexcept((coord <=> 0) > 0))
	{
		auto rv = coord <=> 0;
		if(rv < 0) return 1;
		if(rv > 0) return 3;
		return 2;
	}

	static constexpr value_type normalize(bool coord) noexcept { return coord? 3 : 2; }

	static constexpr value_type op(value_type accum, int coord) noexcept { return accum * 4 + coord; }

	template<std::size_t... is, class... Coords>
	static constexpr value_type fold(std::index_sequence<is...>, Coords... coords)
	noexcept((noexcept(normalize(coords)) && ...))
	{
		if constexpr(sizeof...(Coords) == 0) return {};
		else return ((normalize(coords) << (is * 2)) | ...);
	}

	template<std::size_t... is>
	static constexpr value_type ones(std::index_sequence<is...>) noexcept
	{
		if constexpr(sizeof...(is) == 0) return {};
		else return ((3ull << (is * 2)) | ...);
	}

	struct Compare {
		template<class Left, class Right, std::size_t... is>
		constexpr quadrant operator()(Left &&left, Right &&right, std::index_sequence<is...>)
		noexcept(noexcept(quadrant(std::get<is>(std::forward<Left>(left)) <=> std::get<is>(std::forward<Right>(right))...)))
		{
			return {std::get<is>(std::forward<Left>(left)) <=> std::get<is>(std::forward<Right>(right))...};
		}

		template<class Left, class Right, class Comp, std::size_t... is>
		constexpr quadrant operator()(Left &&left, Right &&right, Comp &&comp, std::index_sequence<is...>)
		noexcept(noexcept(quadrant(comp(std::get<is>(std::forward<Left>(left)), std::get<is>(std::forward<Right>(right)))...)))
		{
			return {comp(std::get<is>(std::forward<Left>(left)), std::get<is>(std::forward<Right>(right)))...};
		}
	};

	value_type value;
	value_type mask;
};

}

#endif
