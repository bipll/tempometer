import QtQuick 2.0
import QtQuick.Shapes 1.15
import 'Format.js' as Fmt
import 'Util.js' as U

Shape {
    property real scaleLevel

    width: 128
    height: 128

    //anchors.fill: parent
    ShapePath {
        strokeColor: 'black'
        fillColor: 'transparent'
        PathLine { x: 0; y: 127 }
        PathLine { x: 127; y: 127 }
    }

    ShapePath {
        strokeColor: 'black'
        startX: 0; startY: 31
        PathLine { relativeX: 2; relativeY: 0 }
    }
    ShapePath {
        strokeColor: 'black'
        startX: 0; startY: 63
        PathLine { relativeX: 8; relativeY: 0 }
    }
    ShapePath {
        strokeColor: 'black'
        startX: 0; startY: 95
        PathLine { relativeX: 2; relativeY: 0 }
    }

    ShapePath {
        strokeColor: 'black'
        startX: 32; startY: 127
        PathLine { relativeX: 0; relativeY: -2 }
    }
    ShapePath {
        strokeColor: 'black'
        startX: 64; startY: 127
        PathLine { relativeX: 0; relativeY: -8 }
    }
    ShapePath {
        strokeColor: 'black'
        startX: 96; startY: 127
        PathLine { relativeX: 0; relativeY: -2 }
    }

    Text {
        x: 16
        y: 100
        text: Fmt.dist(scaleLevel)
        style: Text.Outline
        styleColor: U.outlineColor
    }
}
