import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0

import 'Util.js' as U

Item {
    id: trax
    width: parent.width
    height: parent.height

    signal back
    signal review
    signal unhighlighted
    signal forget
    signal goOn
    signal repeat

    property string selectedTrack
    property int selectedLap
    property int laps
    property bool reverse

    property int fontSize: 19
    property string highlightedTrack: ''
    property int hightlightedLap: 0
    property int topRow: -1

    property color highlightColor
    property int currentlyHighlighted: -1
    Flickering {
        target: trax
        property: 'highlightColor'
        to: 'lightgrey'
        running: currentlyHighlighted >= 0
    }
    onHighlightColorChanged: {
        let model = oldTracks.model
        for(let i = 0; i < model.columnCount; ++i) {
            model.setData(model.index(currentlyHighlighted, i), 'background', highlightColor)
        }
    }

    TableView {
        id: columnHeaders
        rowSpacing: 10
        columnSpacing: 5
        x: 0
        width: trax.width
        y: 0
        height: 18

        syncView: oldTracks
        syncDirection: Qt.Horizontal

        property var headline: ['date', 'type', 'laps', 'lap', 'dist', 'time', 'avg', 'mtime', 'mavg', 'comment']
        model: TableModel {
            TableModelColumn { display: 'date' }
            TableModelColumn { display: 'type' }
            TableModelColumn { display: 'laps' }
            TableModelColumn { display: 'lap' }
            TableModelColumn { display: 'dist' }
            TableModelColumn { display: 'time' }
            TableModelColumn { display: 'avg' }
            TableModelColumn { display: 'mtime' }
            TableModelColumn { display: 'mavg' }
            TableModelColumn { display: 'comment' }

            rows: [columnHeaders.headline.reduce((hdrs, key) => { hdrs[key] = key; return hdrs }, Object())]
        }
        delegate: Text { text: display; font.pointSize: fontSize }
        Component.onCompleted: model.rows = [headline.reduce((hdrs, key) => { hdrs[key] = key; return hdrs }, Object())]
    }

    TableView {
        id: oldTracks

        rowSpacing: 8
        columnSpacing: 8

        x: 0
        width: trax.width
        y: columnHeaders.height + 10
        height: trax.height - y

        model: TableModel {
            TableModelColumn { display: 'date'; background: 'bg' }
            TableModelColumn { display: 'type'; background: 'bg' }
            TableModelColumn { display: 'laps'; background: 'bg' }
            TableModelColumn { display: 'lap' ; background: 'bg' }
            TableModelColumn { display: 'dist'; background: 'bg' }
            TableModelColumn { display: 'time'; background: 'bg' }
            TableModelColumn { display: 'avg'; background: 'bg' }
            TableModelColumn { display: 'mtime'; background: 'bg' }
            TableModelColumn { display: 'mavg'; background: 'bg' }
            TableModelColumn { display: 'comment'; background: 'bg' }

            rows: []
        }

        columnWidthProvider: (column) => expandedRow === 0 && column === 3? 0 : -1

        property color origColor

        function unhighlight(row = -1) {
            if(currentlyHighlighted >= 0 && currentlyHighlighted !== row) {
                for(let i = 0; i < model.columnCount; ++i) {
                    model.setData(model.index(currentlyHighlighted, i), 'background', origColor)
                }
                currentlyHighlighted = -1
                unhighlighted()
            }
        }

        function highlight(row) {
            if(currentlyHighlighted === row) return
            unhighlight()
            origColor = model.data(model.index(row, 0), 'background')
            currentlyHighlighted = row
        }

        delegate: Text {
            id: tableCell
            text: display// + (control? 'CTRL' : '')
            font.pointSize: fontSize
            font.family: 'Droid Sans Mono'
            Rectangle {
                id: shadow
                width: row === oldTracks.expandedRow + oldTracks.extraRows - 1? parent.width + oldTracks.columnSpacing : parent.width
                height: parent.height + oldTracks.rowSpacing
                //anchors.fill: parent
                color: background
                z: -1
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    oldTracks.unhighlight()
                    let forExpansion = true
                    let forMenu = false
                    let rowClicked = row
                    let displayedRows = oldTracks.model.rows
                    let changed = false
                    console.log('clicked at', rowClicked, 'expanded from', oldTracks.expandedRow, 'for', oldTracks.extraRows)
                    if(oldTracks.expandedRow > 0) {
                        if(row < oldTracks.expandedRow || row >= oldTracks.expandedRow + oldTracks.extraRows) {
                            console.log('removing from', oldTracks.expandedRow, 'for', oldTracks.extraRows)
                            displayedRows.splice(oldTracks.expandedRow, oldTracks.extraRows)
                            let wasExpandedAt = oldTracks.expandedRow
                            let expansionHeight = oldTracks.extraRows
                            if(wasExpandedAt === row + 1) {
                                oldTracks.expandedRow = 0
                                oldTracks.extraRows = 0
                                forExpansion = false
                            }
                            else if(row > wasExpandedAt) rowClicked -= expansionHeight
                            changed = true
                        }
                        else {
                            forExpansion = false
                            forMenu = true
                        }
                    }

                    if(forExpansion) {
                        let track = trax.tracks[rowClicked]
                        let laps = track.laps.length
                        if(laps > 1) {
                            oldTracks.expandedRow = rowClicked + 1
                            oldTracks.extraRows = laps + 1
                            displayedRows.splice(oldTracks.expandedRow, 0, ...track.laps.map((header) => Object.assign(header, {bg: 'lightgrey'})),
                                                 oldTracks.dummyRow)
                            changed = true
                        }
                        else {
                            oldTracks.expandedRow = 0
                            oldTracks.extraRows = 0
                        }
                        forMenu = true
                    }
                    else if(!forMenu) contextMenu.close()

                    if(changed) {
                        oldTracks.model.rows = displayedRows
                        oldTracks.forceLayout()
                    }
                    if(forMenu) {
                        oldTracks.highlight(clickedRow)
                        contextMenu.popup()
                    }
                }
                onPressAndHold: {
                    oldTracks.highlight(row)
                    contextMenu.popup()
                }

                Menu {
                    id: contextMenu
                    MenuItem {
                        text: "Review"
                        font.pointSize: fontSize
                        onTriggered: {
                            selectedTrack = oldTracks.model.rows[row].id.toFixed(3)
                            selectedLap = Number.parseInt('0' + oldTracks.model.rows[row].lap.trim())
                            laps = Number.parseInt(oldTracks.model.rows[row].laps)
                            review()
                        }
                        Component.onCompleted: background.color = '#befe80'
                    }
                    MenuItem {
                        text: "Delete"
                        font.pointSize: fontSize
                        onTriggered: askOnceAgain(row, oldTracks.model.rows[row].id.toFixed(3), Number.parseInt('0' + oldTracks.model.rows[row].lap.trim()))
                    }
                }
            }
        }

        clip: true

        onFlickEnded: forceLayout()

        function collapseRows() {
            if(expandedRow > 0) {
                console.log('Collapsing')
                let rows = oldTracks.model.rows
                rows.splice(oldTracks.expandedRow, oldTracks.extraRows)
                oldTracks.model.rows = rows
                oldTracks.expandedRow = 0
                oldTracks.forceLayout()
            }
        }

        function remove(row) {
            console.log('Removing, row =', row, expandedRow + ':' + extraRows)
            let rows = model.rows
            if(row === expandedRow - 1) {
                rows.splice(row, extraRows + 1)
                expandedRow = extraRows = 0
            }
            else if(row === expandedRow && extraRows === 2) {
                rows.splice(row, 2)
                expandedRow = extraRows = 0
            }
            else {
                rows.splice(row, 1)
                if(row >= expandedRow && row < expandedRow + extraRows) --extraRows
                else if(row < expandedRow) --expandedRow
            }
            model.rows = rows
            unhighlight()
            forceLayout()
        }

        property var dummyRow: columnHeaders.headline.reduce((dr, key) => { dr[key] = ' '; return dr }, {rowType: 1, bg: 'transparent'})

        property int expandedRow: 0
        property int extraRows: 0
    }

    property var tracks: []
    function inflate() {
        let newTracks = tracks.length > 0? db.tracks(tracks[0].header.id) : db.tracks()
        console.log('newTracks:', newTracks)
        if(newTracks.length === 0) return
        tracks.splice(0, 0, ...newTracks)
        let rows = oldTracks.model.rows
        rows.splice(0, 0, ...newTracks.map(function(track) {
            let header = track.header
            header.laps = header.laps.padEnd(4, ' ')
            header.rowType = 0
            header.bg = 'transparent'
            return header
        }))
        if(currentlyHighlighted >= 0) currentlyHighlighted += newTracks.length
        if(oldTracks.expandedRow > 0) oldTracks.expandedRow += newTracks.length
        oldTracks.model.rows = rows
        oldTracks.forceLayout()
    }

    Dialog {
        anchors.centerIn: parent
        id: deleteThis
        property string track
        property int lap
        property int row
        Label {
            text: 'Delete this ' + (deleteThis.lap === 0? 'track' : 'lap') + '?'
        }
        standardButtons: Dialog.Yes | Dialog.No

        onAccepted: deleteIndeed(row, track, lap)
    }

    function askOnceAgain(row, track, lap) {
        deleteThis.track = track
        deleteThis.lap = lap
        deleteThis.row = row
        deleteThis.open()
    }

    function deleteIndeed(row, track, lap) {
        db.forget(track, lap)
        replay.tape = track
        replay.forget(lap)
        if(lap === 0) tracks.splice(row, 1)
        else {
            let removedTrack = tracks[oldTracks.expandedRow - 1]
            removedTrack.laps.splice(row - oldTracks.expandedRow, 1)
        }
        oldTracks.remove(row)
    }
}
