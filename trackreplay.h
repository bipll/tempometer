#ifndef TRACKREPLAY_H
#define TRACKREPLAY_H

#include <type_traits>
#include <QObject>
#include <QDateTime>
#include <QGeoCoordinate>
#include <QDir>
#include <QFile>
#include <QTimer>

#include <rep_tracksegment_source.h>
#include <rep_tracksegment_replica.h>

#include "files.h"

class TrackReplay : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString tape READ tape WRITE insertTape NOTIFY tapeChanged)
    Q_PROPERTY(quint32 trackNo READ trackNo WRITE setTrackNo NOTIFY trackNoChanged)
    Q_PROPERTY(qint64 timestamp READ timestamp NOTIFY timestampChanged)
public:
    explicit TrackReplay(QObject *parent = nullptr);

    quint32 trackNo() const { return tn; }
    void setTrackNo(quint32 trackNo) {
        if(tn != trackNo) {
            tn = trackNo;
            emit trackNoChanged(tn);
        }
    }

    qint64 timestamp() const { return ts; }

    QString tape() const { return tp; }
    void insertTape(QString const &tape) {
        qDebug() << "Inserting tape" << tape;
        if(tp != tape) {
            tp = tape;
            dir.setPath(files::mainFolder.absoluteFilePath(tp));
            emit tapeChanged(tp);
        }
    }

signals:
    void tapeChanged(QString const &tape);
    void trackNoChanged(quint32 trackNo);
    void timestampChanged(qint64 timestamp);
    void nextPoint(qint64 when, QGeoCoordinate const &where);

public slots:
    void fastForward(quint32 laps);
    void replayLap(quint32 lap, bool fast = false);
    void forget(quint32 lap);

private:
    template<class T> bool read(T &rv) {  // :/
        static_assert(std::is_trivial<T>::value && std::is_standard_layout<T>::value);
        return lap.read(reinterpret_cast<char *>(&rv), sizeof(rv)) == sizeof(rv);
    }

    QString tp;
    QDir dir;
    quint32 tn{};
    qint64 ts{};
    QFile lap;
    QTimer transpose;
};

#endif // TRACKREPLAY_H
