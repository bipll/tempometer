#ifndef LOCATIONSOURCE_H_7ba7121af34efa9548a25a75f0d8e9147a6f11c1
#define LOCATIONSOURCE_H_7ba7121af34efa9548a25a75f0d8e9147a6f11c1

#include <QObject>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QTimer>

#include <memory>

class LocationSource : public QObject
{
    Q_OBJECT
public:
    explicit LocationSource(QObject *parent = nullptr);

signals:
    void positionUpdated(QGeoPositionInfo const &);
    void signalLost();

private slots:
    void recharge();

private:
    std::unique_ptr<QGeoPositionInfoSource> raw;
    QTimer signalWatch;
};

#endif
