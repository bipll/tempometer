#ifndef PMR_H
#define PMR_H

#include <iterator>
#include <map>
#include <tuple>

#include "defs.h"

namespace pmr {

template<class Value>
struct Tree {
	void insert(qreal lat, qreal lon, Value v);

private:
	using Key = std::uint64_t;
	using Point = std::tuple<qreal, qreal, Value>;
	using Cell = std::vector<Value>;
	using Impl = std::map<Key, Cell>;
	using Itr = typename Impl::iterator;
	// TODO: add extra code for poles

	Impl impl{{Key{}, Cell{}}};

	Key makeKey(qreal lat, qreal lon);
	bool split(Itr itr);
};

template<class Value>
inline void Tree<Value>::insert(qreal lat, qreal lon, Value v) {
	auto key = makeKey(lat, lon);
	auto cellItr = find(key);
	while(cellItr->second.size() == MAX_POP && split(cellItr)) {
		for(++cellItr; cellItr != impl.end() && cellItr->first <= key; ++cellItr);
		--cellItr;
	}
	if constexpr(std::is_trivial_v<Value>) cellItr->second.emplace_back(lat, lon, v);
	else cellItr->second.emplace_back(lat, lon, std::move(v));
}

template<class Value>
inline Tree<Value>::Key Tree<Value>::makeKey(qreal lat, qreal lon) {
	lat += 90;
	std::int64_t x = lon * defs::coordScale;
	std::int64_t y = lat * defs::coordScale;
}

#endif // PMR_H
