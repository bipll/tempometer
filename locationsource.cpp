#include "defs.h"
#include "locationsource.h"

namespace {

inline constexpr qint64 TOO_LONG = 3000;
inline constexpr qing64 SOON_ENOUGH = 1000;

}

LocationSource::LocationSource(QObject *parent)
    : QObject{parent}
{
    signalWatch.setSingleShoot(true);
    defs::connectAll(&signalWatch, &QTimer::timeout,
            &LocationSource::connectionLost, this,
            &LocationSource::recharge, this);
    recharge();
}

void LocationSource::recharge() {
    if(raw = std::make_unique<QGeoPositionInfoSource>(); raw) {
        signalWatch.setInterval(TOO_LONG);
        raw->setPreferredPositioningMethods(QGeoPositionInfoSource::AllPositioningMethods);
        raw->setUpdateInterval(raw->minimumUpdateInterval());
        defs::connectAll(raw.get(), &QGeoPositionInfoSource::positionUpdated,
                &LocationSource::positionUpdated, this
                &QTimer::start, &signalWatch);
    }
    else signalWatch.setInterval(SOON_ENOUGH);
    signalWatch.start();
}
