.pragma library

.import 'Util.js' as U

function forbiddenLayer(layer) {
    return layer > 0 && layer < U.mapLayers && layer % 3 === 0? layer - 1 : -1
}

class MapLayer {
    constructor(map, maxLayer = U.mapLayers, visible = true) {
        this.map = map
        this.visible = visible
        this.maxLayer = maxLayer
        this.forbiddenLayer = forbiddenLayer(maxLayer)
        this.items = []
        //map.itemLayers.push(this)
    }

    focus() { this.map.focusAt(this) }

    clear() { for(let item of this.items) { console.log('removing item', item, 'from map', this.map); this.map.hide(item); } this.items = [] }

    toggle(layer) {
        if(layer > this.maxLayer || layer === this.forbiddenLayer) this.hide()
        else this.show()
    }

    hide() {
        if(this.visible) {
            this.visible = false
            for(let item of this.items) this.map.hide(item)
        }
    }

    show() {
        if(!this.visible) {
            this.visible = true
            for(let item of this.items) this.map.show(item)
        }
    }

    add(...items) {
        if(this.visible) for(let item of items) {
            this.items.push(item)
            this.map.show(item)
        }
        else for(let item of items) this.items.push(item)
    }

    addOnce(...items) {
        if(this.visible) for(let item of items) {
            if(this.items.indexOf(item) < 0) {
                this.items.push(item)
                this.map.show(item)
            }
        }
        else for(let item of items) if(this.items.indexOf(item) < 0) this.items.push(item)
    }

    remove(...items) {
        if(items.length === 1) {
            let idx = this.items.indexOf(items[0])
            if(idx < 0) return
            let removed = this.items.splice(idx, 1)
            if(this.visible) this.map.hide(removed[0])
        }
        else {
            let filter = new Set()
            for(let item of items) filter.add(item)
            for(let i = 0; i < this.items.length && filter.size > 0;) {
                if(filter.delete(this.items[i])) {
                    for(let count = 1; filter.delete(this.items[i + count]); ++count);
                    this.items.splice(i, count)
                }
                else ++i
            }
        }
    }
}
