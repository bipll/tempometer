#ifndef SAMPLE_H_2df915155eccbffe5ec576ba172e27fd0eaafff1
#define SAMPLE_H_2df915155eccbffe5ec576ba172e27fd0eaafff1

#include <iostream>

#include <array>
#include <utility>

#include <3rdparty/quadrant.cpp/include/quadrant/quadrant.h>
#include <3rdparty/quadrant.cpp/include/quadrant/tuple.h>

namespace smp {

inline constexpr std::size_t HALF_SIZE = 3;
inline constexpr std::size_t DEPTH = HALF_SIZE * 2 + 1;
template<class T, std::size_t depth = DEPTH> using Sample = std::array<T, depth>;


template<std::size_t depth = DEPTH>
inline constexpr std::size_t previous(std::size_t i) noexcept
{
    if(i == 0) [[unlikely]] return depth - 1;
    return i - 1;
}

template<std::size_t depth = DEPTH>
inline constexpr std::size_t previous(std::size_t i, std::size_t dist) noexcept
{
    if(i < dist) return i + depth - dist;
    return i - dist;
}

template<std::size_t depth = DEPTH>
inline constexpr std::size_t inc(std::size_t &i) noexcept
{
    if(++i == depth) [[unlikely]] i = 0;
    return i;
}


template<class... AuxType> struct AuxValue;

template<class AuxType>
struct AuxValue<AuxType>
{
    using aux_type = AuxType;

    constexpr void put(std::size_t i, aux_type value) noexcept {
        storage[i] = value;
    }

    constexpr aux_type at(std::size_t i) const noexcept {
        return storage[i];
    }

protected:
    Sample<aux_type> storage;
};

template<>
struct AuxValue<>
{
    static constexpr void put(std::size_t) noexcept {}
};


template<class T, class... AuxTypes>
requires(sizeof...(AuxTypes) <= 1)
struct Median: AuxValue<AuxTypes...>
{
    using value_type = nstd::cond_t<nstd::invoke<nstd::is_zero, nstd::value_box<sizeof...(AuxTypes)>>,
	    T,
	    std::tuple<T, AuxTypes...>>;

    constexpr Median() noexcept = default;

    constexpr void push(T value, AuxTypes... auxValues) noexcept;
    constexpr void pop() noexcept;
    constexpr bool empty() const noexcept {
        return size == 0;
    }
    constexpr operator bool() const noexcept { return size & 1; }

    constexpr void poppop() noexcept {
        pop();
        if(!*this) pop();
    }

    constexpr value_type value() const noexcept {
	    return {pivot, AuxValue<AuxTypes>::at(previous(i, (size + 1) >> 1))...};
    }

    friend std::ostream &operator<<(std::ostream &s, Median const &m) {
        s << "Median{[";
        for(std::size_t j{}; j < m.window.size(); ++j) {
            if(j) [[likely]] s << ", ";
            if(j == m.i) [[unlikely]] s << '<';
            s << m.window[j];
            if(j == m.i) [[unlikely]] s << '>';
        }
        s << "]; {";
        if(m.size) for(std::size_t j{}; j < (m.size - 1) / 2; ++j) s << (j? ", " : "") << m.left[j];
        s << "} [" << m.pivot << "] {";
        for(std::size_t j{}; j < m.size / 2; ++j) s << (j? ", " : "") << m.right[j];
        return s << "}}";
    }

private:
    static constexpr std::size_t depth = DEPTH;
    std::size_t i{};
    std::size_t size{};
    Sample<T, depth> window;
    T pivot;
    Sample<T, 3> left, right;
};

inline constexpr auto shift(auto a, auto &b) noexcept {
    return std::exchange(b, a);
}

inline constexpr auto shift(auto a, auto &b, auto &...cde) {
    return shift(std::exchange(b, a), cde...);
}

inline constexpr void place(auto, auto x, auto &a) noexcept {
    a = x;
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b) noexcept {
    if(cmp(x, a)) shift(x, a, b);
    else shift(x, b);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c) noexcept {
    if(cmp(x, b)) {
        if(cmp(x, a)) shift(x, a, b, c);
        else shift(x, b, c);
    }
    else shift(x, c);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c, auto &d) noexcept {
    if(cmp(x, b)) {
        if(cmp(x, a)) shift(x, a, b, c, d);
        else shift(x, b, c, d);
    }
    else if(cmp(x, c)) shift(x, c, d);
    else shift(x, d);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c, auto &d, auto &e) noexcept {
    if(cmp(x, c)) {
        if(cmp(x, b)) {
            if(cmp(x, a)) shift(x, a, b, c, d, e);
            else shift(x, b, c, d, e);
        }
        else shift(x, c, d, e);
    }
    else place(cmp, x, d, e);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c, auto &d, auto &e, auto &f) noexcept {
    if(cmp(x, c)) {
        if(cmp(x, b)) {
            if(cmp(x, a)) shift(x, a, b, c, d, e, f);
            else shift(x, b, c, d, e, f);
        }
        else shift(x, c, d, e, f);
    }
    else place(cmp, x, d, e, f);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c, auto &d, auto &e, auto &f, auto &g) noexcept {
    if(cmp(x, d)) {
        if(cmp(x, b)) {
            if(cmp(x, a)) shift(x, a, b, c, d, e, f, g);
            else shift(x, b, c, d, e, f, g);
        }
        else if(cmp(x, c)) shift(x, c, d, e, f, g);
        else shift(x, d, e, f, g);
    }
    else place(cmp, x, e, f, g);
}

inline constexpr void place(auto cmp, auto x, auto &a, auto &b, auto &c, auto &d, auto &e, auto &f, auto &g, auto &h) noexcept {
    if(cmp(x, d)) {
        if(cmp(x, b)) {
            if(cmp(x, a)) shift(x, a, b, c, d, e, f, g, h);
            else shift(x, b, c, d, e, f, g, h);
        }
        else if(cmp(x, c)) shift(x, c, d, e, f, g, h);
        else shift(x, d, e, f, g, h);
    }
    else place(cmp, x, e, f, g, h);
}

template<std::size_t min = 0, std::size_t max = 2>
constexpr void sameArray(auto &array, auto removed, auto value) noexcept {
    using qu = nstd::quadrant;
    switch(qu{removed <=> array[1]}) {
        case qu{-1}: place(std::greater{}, value, array[max], array[1], array[min]); break;
        case qu{1}: place(std::less{}, value, array[min], array[1], array[max]); break;
        default: switch(qu{value <=> removed}) {
                     case qu{-1}: place(std::less{}, value, array[min], array[1]); break;
                     case qu{1}: place(std::greater{}, value, array[max], array[1]); break;
                 }
    }
}

template<class T, class... AuxTypes>
requires(sizeof...(AuxTypes) <= 1)
inline constexpr void Median<T, AuxTypes...>::push(T value, AuxTypes... auxValues) noexcept {
    using qu = nstd::quadrant;
    switch(size) {
        case 0: pivot = value;
                break;
        case 1: place(std::less{}, value, pivot, right[0]);
                break;
        case 2: place(std::greater{}, value, right[0], pivot, left[0]);
                break;
        case 3: place(std::less{}, value, left[0], pivot, right[0], right[1]);
                break;
        case 4: place(std::greater{}, value, right[1], right[0], pivot, left[0], left[1]);
                break;
        case 5: place(std::less{}, value, left[1], left[0], pivot, right[0], right[1], right[2]);
                break;
        case 6: place(std::greater{}, value, right[2], right[1], right[0], pivot, left[0], left[1], left[2]);
                break;
        [[likely]] default: break;
    }
    if(size < depth) [[unlikely]] {
        window[i] = value;
        this->put(i++, auxValues...);
        if(i == depth) [[unlikely]] i = 0;
        ++size;
        return;
    }
    T removed = window[i];
    window[i] = value;
    (this->put(i, auxValues), ...);
    if(++i == depth) [[unlikely]] i = 0;
    switch(qu{removed <=> pivot, value <=> pivot}) {
        case qu{-1, -1}:
        case qu{-1, 0}: sameArray<2, 0>(left, removed, value);
                        break;
        case qu{-1, 1}: sameArray<2, 0>(left, removed, pivot);
                        place(std::greater{}, value, right[2], right[1], right[0], pivot);
                        break;
        case qu{1, 1}:
        case qu{1, 0}: sameArray<0, 2>(right, removed, value);
                       break;
        case qu{1, -1}: sameArray<0, 2>(right, removed, pivot);
                        place(std::less{}, value, left[2], left[1], left[0], pivot);
                        break;
        case qu{0, -1}: place(std::less{}, value, left[2], left[1], left[0], pivot);
                        break;
        case qu{0, 1}: place(std::greater{}, value, right[2], right[1], right[0], pivot);
                       break;
    }
}

template<class T, class... AuxTypes>
requires(sizeof...(AuxTypes) <= 1)
inline constexpr void Median<T, AuxTypes...>::pop() noexcept
{
    using qu = nstd::quadrant;
    if(size <= 1) [[unlikely]] {
        i = size = 0;
        return;
    }
    const T removed = window[previous(i, size)];
    switch(size) {
        case 2: if(removed != right[0]) pivot = right[0];
                break;
        case 3: switch(qu{removed <=> pivot}) {
                    case qu{1}: shift(left[0], pivot, right[0]); break;
                    case qu{0}: shift(left[0], pivot); break;
                }
                break;
	case 4: switch(qu::cmp(nstd::tuple::make_repeat<2>(removed), std::tuple{pivot, right[0]})) {
                    case qu{-1, -1}: left[0] = pivot;
                    case qu{0, -1}: shift(right[1], right[0], pivot); break;
                    case qu{0, 0}:
                    case qu{1, 0}: right[0] = right[1];
                }
                break;
	case 5: switch(qu::cmp(nstd::tuple::make_repeat<3>(removed), std::tuple{left[0], pivot, right[0]})) {
                    case qu{0, -1, -1}:
                    case qu{0, 0, -1}:
                    case qu{0, 0, 0}: left[0] = left[1]; break;
                    case qu{1, 0, 0}:
                    case qu{1, 0, -1}: shift(left[1], left[0], pivot); break;
                    case qu{1, 1, 0}: shift(left[1], left[0], pivot, right[0]); break;
                    case qu{1, 1, 1}: shift(left[1], left[0], pivot, right[0], right[1]);
                }
                break;
	case 6: switch(qu::cmp(nstd::tuple::make_repeat<5>(removed), std::tuple{left[0], pivot, right[0], right[1], right[2]})) {
                    case qu{0, 0, 0, 0, -1}:
                    case qu{1, 0, 0, 0, -1}:
                    case qu{1, 1, 0, 0, -1}:
                    case qu{1, 1, 1, 0, -1}: right[1] = right[2]; break;
                    case qu{-1, -1, -1, -1, -1}: left[1] = left[0];
                    case qu{0, -1, -1, -1, -1}: left[0] = pivot;
                    case qu{0, 0, -1, -1, -1}:
                    case qu{1, 0, -1, -1, -1}: pivot = right[0];
                    case qu{0, 0, 0, -1, -1}:
                    case qu{1, 0, 0, -1, -1}:
                    case qu{1, 1, 0, -1, -1}: shift(right[2], right[1], right[0]);
                }
                break;
        default: 
                if(removed > pivot) {
                    if(removed > right[1]) {
                        shift(left[2], left[1], left[0], pivot, right[0], right[1], right[2]);
                    }
                    else if(removed > right[0]) {
                        shift(left[2], left[1], left[0], pivot, right[0], right[1]);
                    }
                    else shift(left[2], left[1], left[0], pivot, right[0]);
                }
                else if(removed > left[1]) {
                    if(removed > left[0]) shift(left[2], left[1], left[0], pivot);
                    else shift(left[2], left[1], left[0]);
                }
                else if(removed > left[2]) left[1] = left[2];
    }
    --size;
};

template<class... Ts, class... AuxTypes>
requires(sizeof...(AuxTypes) <= 1)
class Median<std::tuple<Ts...>, AuxTypes...>
{
    using FormalValueType = std::tuple<Ts...>;
    using LastType = nstd::tuple::last_t<FormalValueType>;

    using Prefix = nstd::tuple::init_t<FormalValueType>;
    using Suffix = std::tuple<LastType, AuxTypes...>;

    static constexpr std::size_t lastIdx = std::tuple_size_v<Prefix>;

public:
    using value_type = nstd::tuple::cat<Prefix, Suffix>;

    constexpr Median() noexcept = default;

    constexpr void push(Ts... values, AuxTypes... aux) noexcept {
        foreach([](auto &dim, auto v, auto... ax) noexcept { dim.push(v, ax...); }, values..., aux...);
    }

    constexpr void pop() noexcept {
        foreach([](auto &dim) noexcept { dim.pop(); });
    }

    constexpr bool empty() const noexcept {
        return last.empty();
    }

    constexpr operator bool() const noexcept { return last; }

    constexpr void poppop() noexcept {
        foreach([](auto &dim) noexcept { dim.poppop(); });
    }

    constexpr value_type value() const noexcept {
        return value(std::make_index_sequence<sizeof...(Ts) - 1>{});
    }

    constexpr auto aux() const noexcept {
        return last.aux();
    }

private:
    template<std::size_t i = 0>
    constexpr void foreach(auto &&f, auto arg, auto... args) noexcept {
        if constexpr(i == lastIdx) f(last, arg, args...);
        else {
            f(std::get<i>(prefix), arg);
            foreach<i + 1>(f, args...);
        }
    }

    template<std::size_t i = 0>
    constexpr void foreach(auto &&f) noexcept {
        if constexpr(i == lastIdx) f(last);
        else {
            f(std::get<i>(prefix));
            foreach<i + 1>(f);
        }
    }

    template<std::size_t... is>
    constexpr value_type value(std::index_sequence<is...>) const noexcept {
        return nstd::tuple::cat{Prefix{std::get<is>(prefix).value()...}, last.value()};
    }

    nstd::tuple::map_t<nstd::fun_box<Median>, Prefix> prefix;
    Median<LastType, AuxTypes...> last;
};

}

#endif
