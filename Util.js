.pragma library

function log(...args) {
    console.log('==========================================', ...args)
}

const l23 = Math.log2(3)

const tooSLow = 0.25
const tooClose = 3

const defaultColor = 'red'
const outlineColor = 'beige'
const lineColors = ['black', 'blue', 'green', 'magenta', 'cadetblue', 'brown', 'cornflowerblue', 'salmon', 'turquoise', 'slategrey', 'purple']

const mapLayers = 10

function lineColor(no) {
    return lineColors[no % lineColors.length]
}

function now() {
    return new Date().getTime()
}

function textWidth(s, letterSpacing = 9) {
    if(s === null || s === undefined) return 0
    if(typeof s !== 'string' && !(s instanceof String)) s = s.toString()
    let rv = 0
    for(let line of s.split('\n')) if(line.length > rv) rv = line.length
    return rv * letterSpacing
}

function push(array, ...objects) {
    array.push(...objects)
    return array
}
