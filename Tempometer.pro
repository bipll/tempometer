QT += core quick quickcontrols2 network location positioning remoteobjects

CONFIG += c++2b qt

QTC_CLANG_CMD_OPTIONS_BLACKLIST=-fno-keep-inline-dllexport

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        androidinit.cpp \
        batterylevel.cpp \
        drunk.cpp \
        filter.cpp \
        locationsource.cpp \
        main.cpp \
        startup.cpp \
        trackcontrol.cpp \
        trackreplay.cpp \
        tracksegment.cpp \
        trackwriter.cpp \
        tst_median.cpp

RESOURCES += qml.qrc

INCLUDEPATH += 3rdparty/eigen

CONFIG += qmltypes
QML_IMPORT_NAME = com.gitlab.bipll.tempometer.batterylevel
QML_IMPORT_MAJOR_VERSION = 1

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QTLOCATION_OPENGL_ITEMS = 1

REPC_SOURCE = tracksegment.rep controlchain.rep

REPC_REPLICA = tracksegment.rep controlchain.rep

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/drawable-ldpi-v11/24px.png \
    android/res/drawable/notification.png \
    android/res/values/libs.xml \
    android/src/com/gitlab/bipll/tempometer/BatteryListener.java \
    android/src/com/gitlab/bipll/tempometer/TrackWriter.java \
    tracksegment.rep

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    3rdparty/quadrant.cpp/include/quadrant/quadrant.h \
    androidinit.h \
    batterylevel.h \
    controlchain.h \
    defs.h \
    defs.h \
    drunk.h \
    files.h \
    filter.h \
    geo.h \
    locationsource.h \
    median.h \
    pmr.h \
    startup.h \
    trackcontrol.h \
    trackreplay.h \
    tracksegment.h \
    trackwriter.h

