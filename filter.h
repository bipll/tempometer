#ifndef FILTER_H_ee5e9e93af78a4422c79899c4a38f18c13e4cdd6
#define FILTER_H_ee5e9e93af78a4422c79899c4a38f18c13e4cdd6

#include <QGeoPositionInfo>
#include <QGeoCoordinate>
#include <QObject>
#include <QTimer>
#include "defs.h"
#ifdef foreach
#   undef foreach
#endif
#include "median.h"

#include <array>
#include <deque>
#include <tuple>
#include <utility>

//#include <Eigen/Core>

class Filter : public QObject
{
    Q_OBJECT

    static constexpr qint64 TIMEOUT = 3000;

    static constexpr std::size_t MAGIC_SIZE_THRESHOLD = 100;
    static constexpr std::size_t MAGIC_SIGMAS_THRESHOLD = 25;
    static constexpr qint64 MAGIC_RIPENESS_THRESHOLD = 500;
    static constexpr std::size_t MAX_SKIP = 20;

public:
    explicit Filter(QObject *parent = nullptr);

public slots:
    void update(QGeoPositionInfo const &);
    void start();
    void lap();
    void pause();
    void unpause();
    void stop();

signals:
    void roughPosition(QGeoPositionInfo const &);
    void exactPosition(QGeoPositionInfo const &);
    void started();
    void lapStarted();
    void paused();
    void unpaused();
    void stopped();

private:
    smp::Median<std::tuple<qreal, qreal, qreal>, qint64> graph;
    QGeoPositionInfo lastSighted;
    // filtered positions
    QGeoPositionInfo past, present, future;
    QTimer timeoutWatch;

    void reset();
    QGeoPositionInfo completeEstimate(QGeoPositionInfo) const;
    QGeoPositionInfo median() const;

    void updated(QGeoPositionInfo);
    void updated();

    QGeoCoordinate lastKnownSpace{}, lastWrongSpace{};
    qint64 lastKnownTime{}, lastWrongTime{}, lastTime{};
    qreal lastKnownSpd{};

    bool full{};

    enum Event {
        Start,
        Lap,
        Pause,
        Unpause,
        Stop
    };
    using Spark = std::pair<qint64, Event>;

    std::deque<Spark> sparks;
    qreal direction, angularSpeed{};

    inline void fuseSpark(Event event) { sparks.emplace_back(defs::now(), event); }

    void spark(Event);
#if 0
    using State = Eigen::Vector<qreal, 2>;
    using HalfState = Eigen::Vector<qreal, 2>;
    using Matrix = Eigen::Matrix<qreal, 2, 2>;
    using QuarterMatrix = Eigen::Matrix<qreal, 2, 2>;
    using Gain = Eigen::Matrix<qreal, 2, 2>;

    qint64 lastTime{defs::nothingl};
    qreal speed, direction;

    State state = State::Zero(), observation = State::Zero();
    QGeoCoordinate zero;
    Matrix p = Matrix::Identity(), r = QuarterMatrix::Identity();
    Gain gain;
    bool filterOn{};
#endif
};

#endif
