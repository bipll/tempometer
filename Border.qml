import QtQuick 2.15
import QtQuick.Shapes 1.15

Shape {
    ShapePath {
        strokeWidth: 3
        strokeColor: '#F0F0F0'
        PathLine { x: width }
    }
}
