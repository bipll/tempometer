.pragma library

.import 'Util.js' as U

const tooSlow = 0.25

function pad(val, size = 2) {
    return val.toString().padStart(size, '0')
}

var speedFactor = 3.6
var mile = 1000
var yard = 1
var meterSuffix = 'm'

function missing(val) { return val === undefined || val === null || isNaN(val) }

function tuple(formatter) {
    return (val) => [formatter(val.expectedValue), 'MAX ' + formatter(val.expectedMax)]
}

function singleValue(formatter) {
    return (val) => [formatter(val.expectedValue), '']
}

function speed(val) {
    if(missing(val)) return '--- '
    if(val < tooSlow) return '.000'
    val *= speedFactor
    if(val < 1) return val.toFixed(3).substr(1)
    if(val < 100) return val.toPrecision(3)
    return Math.round(val).toString()
}
var asSpeed = tuple(speed)

function dist(val, trim = true) {
    if(missing(val)) return '--- '
    val = Math.round(val)
    if(val < mile * 0.8) return pad(val / yard, 3) + meterSuffix
    let miles = val / mile
    if(!trim) return miles.toFixed(3)
    if(miles < 10) return miles.toFixed(2)
    if(miles < 100) return miles.toFixed(1)
    return Math.round(miles).toString()
}
var asDist = singleValue(dist)
var asAlt = tuple(dist)

function time(val, trim = true) {
    if(missing(val) || val < 0) return '--:--'
    let minutes = val / 60000 | 0
    let seconds = val % 60000 | 0
    if(val < 3600000) return `${pad(minutes)}:${pad(seconds / 1000 | 0)}`
    let hours = val / 3600000 | 0
    minutes %= 60
    return trim?
                hours < 10? `${hours}:${pad(minutes)}:${seconds / 10000 | 0}` : `${hours}:${pad(minutes)}` :
                `${hours}:${pad(minutes)}:${(seconds / 10000).toFixed(3)}`
}
var asTime = singleValue(time)

function asCoordinate(coord, plus, minus) {
    if(missing(coord)) return ''
    let sgn = coord >= 10? plus : coord >= 0? plus + ' ' : coord > -10? minus + ' ' : minus
    return sgn + Math.abs(coord).toFixed(5)
}
function asLat(coord) { return asCoordinate(coord, 'N', 'S') }
function asLon(coord) { return asCoordinate(coord, 'E', 'W') }

function asCoordinates(lat, lon, fs = '\n') {
    if(missing(lat) || missing(lon)) return ''
    let latSgn = lat >= 10? 'N' : lat >= 0? 'N ' : lat > -10? 'S ' : 'S'
    let lonSgn = lon >= 10? 'E' : lon >= 0? 'E ' : lon > -10? 'W ' : 'W'
    return latSgn + Math.abs(lat).toFixed(5) + fs + lonSgn + Math.abs(lon).toFixed(5)
}

function asLap(val) {
    if(val === undefined || val === null) return ''
    let time = val.time
    let hours = pad(time / 3600000 | 0)
    let minutes = pad(time % 3600000 / 60000 | 0)
    let seconds = pad(time % 60000 / 1000 | 0)

    return `${val.trackNo.toString().padStart(3, ' ')}. ${hours}:${minutes}:${seconds} ${dist(val.dist)} ${speed(val.avg)}`
}

function asPercentage(val) {
    return Math.round(val).toString().padStart(3, ' ') + '%'
}

const digits = [
                 ['m', 1000],
                 ['cm', 900],
                 ['d', 500],
                 ['cd', 400],
                 ['c', 100],
                 ['xc', 90],
                 ['l', 50],
                 ['xl', 40],
                 ['x', 10],
                 ['ix', 9],
                 ['v', 5],
                 ['iv', 4],
                 ['i', 1]
             ]
const DIGITS = digits.map((kv) => [kv[0].toUpperCase(), kv[1]])

function roman(arabic, alphabet = digits) {
    let rv = ''
    let i = 0
    while(arabic > 0) {
        let digit = alphabet[i++]
        while(arabic >= digit[1]) {
            rv += digit[0]
            arabic -= digit[1]
        }
    }
    return rv
}

function toString(value) {
    if(typeof value === 'string' || value instanceof String) return value
    let rv = value.toFixed(3)
    if(rv[rv.length - 1] === '0') {
        for(var i = rv.length - 1; rv[i] === '0'; --i);
        if(rv[i] !== '.') ++i
        rv = rv.substring(0, i)
    }
    return rv
}
