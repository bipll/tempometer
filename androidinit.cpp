#include <QCoreApplication>

#include <QCoreApplication>
#include <QDebug>
#include <QJniObject>

#include "androidinit.h"

namespace {

void keepOn() {
    QNativeInterface::QAndroidApplication::runOnAndroidMainThread([&]{
        QJniObject activity = QNativeInterface::QAndroidApplication::context();
        if(activity.isValid()) {
            QJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");

            if (window.isValid()) {
                constexpr int FLAG_KEEP_SCREEN_ON = 128;
                window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON);
                activity.callMethod<void>("setShowWhenLocked", "(Z)V", true);
            }
        }
    });
}

}

void androidInit()
{
    keepOn();
}
