.pragma library

.import 'Utils.js' as U

const systems = [
    {name: 'metric', yard: 1, mile: 1000},
    {name: 'imperial', yard: 0.9144, mile: 1609.344},
    {name: 'nautical', yard: 1, mile: 1852}
]

function unitLookup(scale, value) {
    for(let i = 0; i < systems.length; ++i) {
        if(U.close(systems[i][scale], value)) return i
    }
}

const yardLookup = unitLookup.bind(undefined, 'yard')
const mileLookup = unitLookup.bind(undefined, 'mile')
