import QtQuick 2.15
import QtQuick.Controls 2.15
import 'Util.js' as U
import 'Format.js' as Fmt

Column {
    height: battery.height

    property real level
    property real firstLevel
    property real t0: NaN
    property real fontSize
    property real remainingTime

    Timer {
        id: sandclock
        interval: 10000
        repeat: true
        onTriggered: {
            remainingTime -= interval
            prognosis.text = Fmt.time(remainingTime)
            interval = (1 + level / 20) * 1000
        }
    }

    onLevelChanged: {
        if(isNaN(t0)) {
            firstLevel = level
            t0 = U.now()
        }
        else if(level < firstLevel) {
            let now = U.now()
            if(now - t0 > 10000) {
                remainingTime = level / (firstLevel - level) * (now - t0)
                prognosis.text = Fmt.time(remainingTime)
                sandclock.restart()
            }
        }
    }

    Text {
        id: battery
        anchors.right: parent.right
        text: level + '%'
        font.pointSize: fontSize
        horizontalAlignment: Text.AlignRight
    }
    Text {
        id: prognosis
        anchors.horizontalCenter: parent.horizontalCenter
        visible: false
        text: ''
        onTextChanged: visible = true
        font.pointSize: 12
        horizontalAlignment: Text.AlignHCenter
    }

}
