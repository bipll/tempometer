#include "trackreplay.h"

//#include <QtCore/private/qandroidextras_p.h>
#include <QQmlContext>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QRemoteObjectHost>
#include <QRemoteObjectNode>
#include <QtGlobal>
#include <QSharedPointer>
#include <QFile>
#include <QScreen>

#include <androidinit.h>
#include <controlchain.h>
#include <drunk.h>
#include <filter.h>
#include <rep_tracksegment_replica.h>
#include <startup.h>
#include <trackcontrol.h>
#include <tracksegment.h>
#include <trackwriter.h>

#include <QFile>
#include <QStandardPaths>

namespace {
auto startService() {
    /*
    auto activity = QJniObject(QNativeInterface::QAndroidApplication:context());
    QAndroidIntent serviceIntent(activity.object(), "com/gitlab/bipll/tempometer/TrackWriter");
    QJniObject result = activity().callObjectMethod(
                "startForegroundService",
                "(Landroid/content/Intent;)Landroid/content/ComponentName;",
                serviceIntent.handle().object());
    qDebug() << "Tempometer from C++:" << "Intent result: " << result.toString();
    return result;
    */
}
}

void createConnections(
    TrackControl *control,
    QGeoPositionInfoSource *positionInfoSource,
    Filter *filter,
    TrackSegment *expectedTotal,
    TrackSegment *expectedCurrent,
    TrackSegment *total,
    TrackSegment *current
    )
{
    using defs::connectAll;

    connectAll(positionInfoSource, &QGeoPositionInfoSource::positionUpdated,
            &Filter::update, filter);

    connectAll(filter, &Filter::roughPosition,
            &TrackSegment::update, expectedTotal, expectedCurrent);

    connectAll(filter, &Filter::exactPosition,
            &TrackSegment::update, total, current);


    connectAll(control, &TrackControl::started,
            &Filter::start, filter,
            &TrackSegment::start, expectedTotal, expectedCurrent);

    connectAll(filter, &Filter::started,
            &TrackSegment::start, total, current);


    connectAll(control, &TrackControl::lap,
            &Filter::lap, filter,
            &TrackSegment::lap, expectedCurrent);

    connectAll(filter, &Filter::lapStarted, &TrackSegment::lap, current);


    connectAll(control, &TrackControl::paused,
            &Filter::pause, filter,
            &TrackSegment::pause, expectedTotal, expectedCurrent);

    connectAll(filter, &Filter::paused, &TrackSegment::pause, total, current);


    connectAll(control, &TrackControl::unpaused,
            &Filter::unpause, filter,
            &TrackSegment::unpause, expectedTotal,
            &TrackSegment::lap, expectedCurrent);

    connectAll(filter, &Filter::unpaused,
            &TrackSegment::unpause, total,
            &TrackSegment::lap, current);


    connectAll(control, &TrackControl::stopped,
            &Filter::stop, filter,
            &TrackSegment::pause, expectedTotal, expectedCurrent);

    connectAll(filter, &Filter::stopped,
            &TrackSegment::pause, total, current);
}


int runApp(char *argv[]) {
    qDebug() << "Tempometer from C++:" << "Running app";
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    int argc{1};
    QGuiApplication app(argc, argv);

    qmlRegisterType<TrackSegment>("trkseg", 1, 0, "TrackSegment");
    qmlRegisterType<TrackControl>("trkctl", 1, 0, "TrackControl");

    /*
    QAndroidJniObject serviceResult;
    ControlChainSimpleSource controlOutput;
    QRemoteObjectHost controlChannel{QUrl(QStringLiteral("local:control.tempometer"))};
    controlChannel.enableRemoting(&controlOutput);

    QObject::connect(&app, &QCoreApplication::aboutToQuit, &controlOutput, &ControlChainSimpleSource::shutdown);
    */

    QQmlApplicationEngine engine;
    auto context = engine.rootContext();

    auto expected{new TrackSegment(&app, 0, 250)},
        expectedCurrent{new TrackSegment(&app, 1)},
        ghost{new TrackSegment(&app, 1)};
    auto total{new FilteredTrack(&app, 0, 250)},
        current{new FilteredTrack(&app, 1)};
    auto replay{new TrackReplay(&app)};
    auto control{new TrackControl(&app)};

    context->setContextProperty("noTime", qreal(-1ull));
    context->setContextProperty("expected", expected);
    context->setContextProperty("total", total);
    context->setContextProperty("current", current);
    context->setContextProperty("expectedCurrent", current);
    context->setContextProperty("trackControl", control);
    context->setContextProperty("ghost", ghost);
    context->setContextProperty("replay", replay);
    context->setContextProperty("filterLatency", qreal(smp::HALF_SIZE));

    QRemoteObjectHost here(QUrl(QStringLiteral("local:current.tempometer")));
    QRemoteObjectHost there(QUrl(QStringLiteral("local:ghost.tempometer")));
    here.enableRemoting(current);
    there.enableRemoting(ghost);

    startService();

    const QUrl url(QStringLiteral("qrc:/tempometer.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.load(url);
    androidInit();

#if 1
    //QtAndroid::requestPermissionsSync(QStringList{"WRITE_EXTERNAL_STORAGE"});
    using PositionSource = QGeoPositionInfoSource;
    auto positionInfoSource = QGeoPositionInfoSource::createDefaultSource(&app);
    positionInfoSource->setPreferredPositioningMethods(QGeoPositionInfoSource::AllPositioningMethods);
    positionInfoSource->setUpdateInterval(positionInfoSource->minimumUpdateInterval());
#else
    using PositionSource = Drunk;
    Drunk susanin;
    auto positionInfoSource = &susanin;
#endif
    Filter filter;
    QObject::connect(positionInfoSource, &PositionSource::positionUpdated, &filter, &Filter::update);

    positionInfoSource->startUpdates();

    for(TrackSegment *ts: {total, current, expected, expectedCurrent})

    return app.exec();
}

namespace {
/*
TrackSegment *svcdbg{};

void serviceDebug(QtMsgType type, const QMessageLogContext &ctx, QString const &m) {
    if(svcdbg) [[likely]] {
        auto msg = ": " + m;
        if(ctx.function) msg = '(' + (ctx.function + (')' + std::move(msg)));
        svcdbg->printLog(msg);
    }
}
*/
}

int runSvc(int argc, char *argv[]) {
    QAndroidService svc(argc, argv);
    TrackWriter writer(&svc);

    QRemoteObjectNode guest;
    QRemoteObjectNode ghost;
    guest.connectToNode(QUrl(QStringLiteral("local:current.tempometer")));
    ghost.connectToNode(QUrl(QStringLiteral("local:ghost.tempometer")));
    QScopedPointer<TrackSegmentReplica> input{guest.acquire<TrackSegmentReplica>()};
    if(!input) return 0;
    QScopedPointer<TrackSegmentReplica> output{ghost.acquire<TrackSegmentReplica>()};
    writer.listen(input.get());

    try {
        return svc.exec();
    }
    catch(std::exception const &ex) {
        qDebug() << "TEMPOMETER WRITER SERVICE:" << "Exception: " << ex.what();
        throw ex;
    }
    catch(...) {
        qDebug() << "TEMPOMETER WRITER SERVICE: Some exception";
        throw;
    }
}
