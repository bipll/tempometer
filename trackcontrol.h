#ifndef TRACKCONTROL_H
#define TRACKCONTROL_H

#include <QObject>

class TrackControl : public QObject
{
    Q_OBJECT
public:
    explicit TrackControl(QObject *parent = nullptr);

signals:
    void started ();
    void lap     ();
    void paused  ();
    void unpaused();
    void stopped ();
};

#endif // TRACKCONTROL_H
