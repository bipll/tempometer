#include "defs.h"
#include "filter.h"
#include "geo.h"
#include "qdebug.h"

#include <QtMath>
#include <QtNumeric>
//#include <Eigen/LU>

#include <cmath>
#include <utility>

namespace {
inline constexpr qreal TOO_FAR = 100;
inline constexpr qint64 TOO_SOON = 250;
}

Filter::Filter(QObject *parent)
    : QObject{parent}
{
    timeoutWatch.setInterval(TOO_LONG);
    timeoutWatch.setSingleShot(true);
    timeoutWatch.callOnTimeout(this, [&]{ reset(); });
}

QGeoPositionInfo Filter::median() const {
    auto [lat, lon, alt, time] = graph.value();
    return {
        QGeoCoordinate(lat, lon, alt), geo::dateTime(time)
    };
}

void Filter::reset() {
    for(graph.poppop(); !graph.empty(); graph.poppop()) {
        updated(median());
    }
}

void Filter::start  () { fuseSpark(Start  ); }
void Filter::lap    () { fuseSpark(Lap    ); }
void Filter::pause  () { fuseSpark(Pause  ); }
void Filter::unpause() { fuseSpark(Unpause); }
void Filter::stop   () { fuseSpark(Stop   ); }

void Filter::spark(Event event) {
    switch(event) {
        case Start  : emit started   (); break;
        case Lap    : emit lapStarted(); break;
        case Pause  : emit paused    (); break;
        case Unpause: emit unpaused  (); break;
        case Stop   : emit stopped   (); break;
    }
}

void Filter::updated(QGeoPositionInfo info) {
    smp::shift(std::move(info), future, present, past);
    updated();
}

QGeoPositionInfo Filter::completeEstimate(QGeoPositionInfo where) const {
    if(!where.isValid() || !lastSighted.isValid()) return where;
    if(!geo::has(where, geo::HSpd)) geo::set(where, geo::HSpd, geo::speed<geo::H>(where, lastSighted));
    if(!geo::has(where, geo::VSpd)) geo::set(where, geo::VSpd, geo::speed<geo::V>(where, lastSighted));
    if(!geo::has(where, geo::Dir)) geo::set(where, geo::Dir, geo::dir(where, lastSighted));
    return where;
}

void Filter::updated() {
    geo::set(present, geo::HSpd, geo::speed<geo::H>(past, present, future));
    geo::set(present, geo::VSpd, geo::speed<geo::V>(past, present, future));
    geo::set(present, geo::Dir, geo::dir(past, present, future));
    auto when = geo::timestamp(present);
    if(!sparks.empty() && sparks.front().first < when) {
        if(geo::isValid<geo::H>(present) && geo::isValid<geo::H>(past)) [[likely]] {
            auto hDist = geo::dist<geo::H>(past, present);
            auto azimuth = geo::dir(present, past);
            auto lastTime = geo::timestamp(past);
            qreal dt = when - lastTime;
            auto lastHSpd = geo::speed<geo::H>(past), lastVSpd = geo::speed<geo::V>(past);
            auto dhSpd = geo::speed<geo::H>(present) - lastHSpd;
            auto dvSpd = geo::speed<geo::V>(present) - lastVSpd;
            qreal z, dz;
            const bool vertical = geo::type(past, present) >= QGeoCoordinate::Coordinate3D;
            if(vertical) {
                z = geo::get(past, geo::Alt);
                dz = geo::get(past, geo::Alt) - z;
            }
            auto const &before = geo::coord(past);
            while(!sparks.empty() && sparks.front().first < when) {
                auto [time, event] = sparks.front();
                const auto factor = (time - lastTime) / dt;
                auto sparkLoc = before.atDistanceAndAzimuth(hDist * factor, azimuth);
                if(vertical) sparkLoc.setAltitude(z + dz * factor);
                emit exactPosition(
                    geo::positionInfo(
                        sparkLoc,
                        geo::dateTime(time),
                        geo::HSpd, lastHSpd + dhSpd * factor,
                        geo::VSpd, lastVSpd + dvSpd * factor
                        )
                    );
                spark(event);
                sparks.pop_front();
            }
        }
    }
    emit exactPosition(present);
    if(!sparks.empty() && sparks.front().first == when) [[unlikely]] {
        spark(sparks.front().second);
        sparks.pop_front();
    }
    past = std::exchange(present, future);
}

void Filter::update(QGeoPositionInfo const &where) {
    completeEstimate(where);
    emit roughPosition(where);
    if(!where.isValid()) {
        reset();
        return;
    }
    auto when = geo::timestamp(where);
    if(when >= lastTime + TIMEOUT) reset();
    auto const &loc = geo::coord(where);

    graph.push(loc.latitude(), loc.longitude(), loc.altitude(), when);
    if(graph) [[likely]] updated(median());
    lastSighted = where;
    lastTime = when;
}
