import QtQuick 2.15
import 'Format.js' as Fmt
import QtQml.Models 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Item {
    id: editRt

    property int population

    width: 0
    visible: false

    Component {
        id: dragDelegate

        MouseArea {
            id: dragArea

            property bool held: false

            anchors { left: parent.left; right: parent.right }
            height: content.height

            drag.target: held ? content : undefined
            drag.axis: Drag.YAxis

            property int x0
            property bool expect: false
            property int threshold: 30
            onPressAndHold: { held = true; expect = false }
            onPressed: { x0 = mouse.x; expect = true }
            onReleased: {
                if(expect && mouse.x - x0 >= threshold) {
                    main.derouteAt(index)
                    listModel.remove(index)
                }
                else held = false
                expect = false
            }

            Rectangle {
                id: content
                height: 36

                anchors { left: parent.left; right: parent.right }

                border.width: 1
                border.color: "lightsteelblue"

                Drag.active: dragArea.held
                Drag.source: dragArea
                Drag.hotSpot.x: width / 2
                Drag.hotSpot.y: height / 2

                radius: 2
                color: dragArea.held ? "lightsteelblue" : "white"
                Behavior on color { ColorAnimation { duration: 100 } }
                states: State {
                    when: dragArea.held

                    ParentChange { target: content; parent: editRt }
                    AnchorChanges {
                        target: content
                        anchors { horizontalCenter: undefined; verticalCenter: undefined }
                    }
                }
                Text { id: text; anchors.fill: parent; font.pointSize: 32; text: Fmt.roman(value, Fmt.DIGITS) }
            }
            DropArea {
                anchors { fill: parent; margins: 10 }

                onEntered: {
                    visualModel.items.move(drag.source.DelegateModel.itemsIndex, dragArea.DelegateModel.itemsIndex)
                    main.swapPois(drag.source.DelegateModel.itemsIndex, dragArea.DelegateModel.itemsIndex)
                }
            }
        }
    }

    ListModel {
        id: listModel
    }

    DelegateModel {
        id: visualModel

        model: listModel
        delegate: dragDelegate
    }

    Button {
        id: left
        height: leftIcon.implicitHeight + 10
        anchors.top: parent.top
        width: parent.width
        Image {
            id: leftIcon
            anchors.margins: 5
            source: 'images/left'
            mirror: true
            fillMode: Image.Pad
        }
        onClicked: popdown()
        Component.onCompleted: background.color = 'transparent'
    }

    ListView {
        id: view

        //y: left.heigth + 20
        anchors { fill: parent; topMargin: left.height + 20; leftMargin: 2; bottomMargin: 2; rightMargin: 2 }

        model: visualModel //[...Array(population).keys()]

        spacing: 4
        cacheBuffer: 50
    }

    function popup() {
        listModel.clear()
        for(let i = 1; i <= population; ++i) listModel.append({value: i})

        visible = true
        width = 100
    }

    Behavior on width { NumberAnimation {} }

    function popdown() {
        width = 0
        visible = false
        listModel.clear()
    }
}
