import QtQuick 2.0
import QtQml 2.15
import 'Util.js' as U
import QtPositioning 5.12
import QtLocation 5.15

import trkseg 1.0
import QtRemoteObjects 5.15

MapPolyline {
    id: lap
    property int trackNo
    Component.onCompleted: trackNo = current.trackNo

    line.width: 12
    line.color: U.defaultColor
    //backend: MapPolyline.OpenGLLineStrip

    property int lookahead: 0
    property bool recordingLap: false
    property int filteredPoints: 0
    property int recordedPoints: 0
    property var end

    path: []

    Connections {
        target: current

        function onLapStarted(when) {
            recordingLap = true
            console.log('Lap started:', recordingLap)
        }
        function onLapEnded(when) {
            console.log('Lap ended')
            recordingLap = false
            target = null
        }

        function onExpectedCoordChanged(coord) {
            console.log('==================================== Coord expected', coord.latitude, coord.longitude, recordingLap, 'recorded =', recordedPoints, 'filtered =', filteredPoints, 'length =', pathLength())
            lap.addCoordinate(coord)
            lap.line.width = 12
        }

        function onCoordChanged(coord) {
            if(!recordingLap) return
            console.log('==================================== Coord changed', coord.latitude, coord.longitude, recordingLap, 'recorded =', recordedPoints, 'filtered =', filteredPoints, 'length =', pathLength())
            if(filteredPoints < lap.pathLength()) lap.replaceCoordinate(filteredPoints++, coord)
            else {
                lap.addCoordinate(coord)
                filteredPoints = lap.pathLength()
            }
            lap.line.width = 12
        }

        function onLastKnownAtChanged(lastKnownAt) {
            if(!recordingLap) return
            console.log('==================================== LastKnown changed', lastKnownAt.latitude, lastKnownAt.longitude, recordingLap, 'recorded =', recordedPoints, 'filtered =', filteredPoints, 'length =', pathLength())
            if(recordedPoints < lap.pathLength()) {
                for(let i = recordedPoints; i < filteredPoints; ++i) lap.removeCoordinate(recordedPoints)
                lap.replaceCoordinate(recordedPoints++, lastKnownAt)
            }
            else {
                lap.addCoordinate(lastKnownAt)
                recordedPoints = lap.pathLength()
            }
            lap.line.width = 12

            filteredPoints = recordedPoints
        }

        function ofFixChanged(fix) {
            if(!fix) {
                while(lap.pathLength() > recordedPoints) lap.removeCoordinate(recordedPoints)
            }
        }
    }

    function archive() {
        lap.line.width = 8
        lap.line.color = U.lineColor(lap.trackNo)
        return this
    }
}

/*
current {
    id: lap

    property string domain

    property real lapStart
    property real lastTime
    property real time: NaN
    property real mtime: NaN

    function checkTime(now) {
        let dt = now - lastTime
        time += dt
        if(spd > U.tooSlow) mtime += dt
        lastTime = now
    }

    property Timer clock: Timer {
        interval: 1000
        repeat: true
        onTriggered: {
            let now = U.now()
            checkTime(now)
            interval = 1000 - (now - lapStart) % 1000
        }
    }

    property Value avg: Value { value: time > 5000? dist * 1000 / time : NaN; time: lap.time }
    property Value mavg: Value { value: mtime > 5000? dist * 1000 / mtime : NaN; time: lap.mtime }
    property Value speed: Value { value: spd; active: recording; time: lap.time }

    function startClock() {
        time = 0
        mtime = 0
        lapStart = U.now()
        lastTime = lapStart
        clock.restart()
    }

    onRecordingChanged: recording? startClock() : clock.stop()
    onTrackNoChanged: if(recording) {
                          checkTime(U.now())
                          startClock()
                      }
                      else clock.stop()

    function archive() {
        return {
            timestamp: lap.timestamp,
            trackNo: lap.trackNo,
            dist: lap.dist,
            time: lap.time,
            avg: {value: lap.avg.value + 0},
            mtime: lap.mtime,
            mavg: {value: lap.mavg.value + 0}
        }
    }

    function stop() { pause() }
}
*/
