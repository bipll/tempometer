#include "batterylevel.h"

#include <QCoreApplication>
#include <QJniEnvironment>
#include <QJniObject>

BatteryLevel::BatteryLevel(QObject *parent)
    : QObject(parent)
{
    setLevel(read());
}

uint BatteryLevel::level() {
    uint now = read();
    if(now != currLevel) setLevel(now);
    return now;
}

void BatteryLevel::update() { level(); }

void BatteryLevel::setLevel(uint newValue) {
    if(newValue == currLevel) return;
    currLevel = newValue;
    emit levelChanged(newValue);
}

uint BatteryLevel::read() const {
    QJniEnvironment env;
    jclass javaClass = env.findClass("com/gitlab/bipll/tempometer/BatteryListener");
    QJniObject classObject(javaClass);
    return QJniObject::callStaticMethod<jdouble>(javaClass,
                                                 "level",
                                                 "(Landroid/content/Context;)D",
                                                 QNativeInterface::QAndroidApplication::context());
}
