#include <chrono>
#include <cmath>

#include <QGeoCoordinate>
#include <QGeoPositionInfoSource>
#include <QObject>
#include <QStandardPaths>
#include <QJniEnvironment>
#include <QJniObject>
#include <QString>
#include <QDir>
#include <QStandardPaths>

#include "trackwriter.h"
#include "files.h"
#include "defs.h"

inline constexpr std::chrono::milliseconds AUTONOMY_DELAY{3000};

TrackWriter::TrackWriter(QCoreApplication *parent): Super(parent), enabled(files::mainFolder.exists()) {
    heartbeat.setSingleShot(true);
    heartbeat.setInterval(AUTONOMY_DELAY);
}

void TrackWriter::openStdOut() {
    /*
    if(enabled) [[likely]] {
        stdOut.setFileName(files::filepath(lapNo));
        stdOut.open(QIODevice::WriteOnly);
        write(files::FILE_FORMAT);
        write(defs::now());
    }
    */
}

void TrackWriter::listen(TrackSegmentReplica *replica) {
    /*
    if(!enabled || !replica) [[unlikely]] return;
    qDebug() << "Tempometer from C++:" << "Bound";
    QObject::connect(replica, &TrackSegmentReplica::trackNoChanged, this,
                     [&](quint32 value) {
                        lapNo = value;
                        if(rec) {
                            stdOut.close();
                            openStdOut();
                        }
                     });
    QObject::connect(replica, &TrackSegmentReplica::timestampChanged, this, [&](quint64 timestamp) { write(timestamp); });
    QObject::connect(replica, &TrackSegmentReplica::recordingChanged, this,
                     [&](bool value) {
                        QJniEnvironment env;
                        jclass javaClass = env.findClass("com/gitlab/bipll/tempometer/TrackWriter");
                        QJniObject classObject(javaClass);
                        QJniObject::callStaticMethod<void>(javaClass, "setStatus", "(I)V", int(value));
                        rec = value;
                        if(rec) {
                            openStdOut();
                            heartbeat.start(AUTONOMY_DELAY);
                        }
                        else stdOut.close();
                     }, Qt::QueuedConnection);
    QObject::connect(replica, &TrackSegmentReplica::whereChanged, this,
                     [&, replica](QGeoPositionInfo const &where) {
                        write(files::DiskPoint<>(where.coordinate(), where.timestamp()));
                        heartbeat.start(AUTONOMY_DELAY);
                     });
    QObject::connect(replica, &TrackSegmentReplica::keep, this, &TrackWriter::keep);
    */
}

void TrackWriter::keep(QString const &dirname, quint32 from, quint32 to) {
    /*
    using namespace files;
    if(!(mainFolder.exists(dirname) || mainFolder.mkdir(dirname))) [[unlikely]] return;
    for(auto i = from; i <= to; ++i) QFile(filepath(i)).rename(filepath(i, dirname));
    */
}
