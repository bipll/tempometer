#ifndef GEO_H_3a855bbaf93ccc2149742c7d1bb25ba14ce0b110
#define GEO_H_3a855bbaf93ccc2149742c7d1bb25ba14ce0b110

#include <algorithm>
#include <concepts>

#include <QGeoPositionInfo>

#include "defs.h"

namespace geo {

enum Attr {
    Dir = QGeoPositionInfo::Direction,
    HSpd = QGeoPositionInfo::GroundSpeed,
    VSpd = QGeoPositionInfo::VerticalSpeed,
    MVar = QGeoPositionInfo::MagneticVariation,
    HAccy = QGeoPositionInfo::HorizontalAccuracy,
    VAccy = QGeoPositionInfo::VerticalAccuracy,
    DAccy = QGeoPositionInfo::DirectionAccuracy,
    Lat,
    Lon,
    Alt
};

enum Geometry { H, V, D3 };

template<Geometry g> inline constexpr Attr geometricSpd = {};
template<> inline constexpr Attr geometricSpd<H> = HSpd;
template<> inline constexpr Attr geometricSpd<V> = VSpd;

inline constexpr auto const &coord(QGeoCoordinate const &rv) noexcept { return rv; }

inline auto coord(QGeoPositionInfo const &info) noexcept { return info.coordinate(); }

inline constexpr qreal lat(auto const &point) noexcept { return coord(point).latitude(); }
inline constexpr qreal lon(auto const &point) noexcept { return coord(point).longitude(); }
inline constexpr qreal alt(auto const &point) noexcept { return coord(point).altitude(); }

inline constexpr auto type(QGeoCoordinate const &coord) noexcept { return coord.type(); }

inline auto type(QGeoPositionInfo const &info) noexcept { return type(coord(info)); }

inline constexpr auto type(auto const &...points) noexcept { return std::min({type(points)...}); }

inline constexpr auto are2D(auto const &...points) noexcept { return (points.isValid() && ... && (type(points...) >= QGeoCoordinate::Coordinate2D)); }
inline constexpr auto are3D(auto const &...points) noexcept { return (points.isValid() && ... && (type(points...) >= QGeoCoordinate::Coordinate3D)); }

template<Geometry g = H>
inline constexpr bool isValid(auto const &point) noexcept {
    if constexpr (g == H) return point.isValid();
    else return are3D(point);
}

inline constexpr bool has(auto const &info, Attr attr) noexcept {
    switch(attr) {
    case Lat:
    case Lon:
        return isValid<H>(info);
    case Alt:
        return isValid<V>(info);
    default:
        if constexpr(requires {{ info.hasAttribute() } -> std::same_as<bool>; }) return info.hasAttribute((QGeoPositionInfo::Attribute)attr);
        else return {};
    }
}

inline constexpr bool has(auto const &info, auto... attr) noexcept {
    return (has(info, attr) && ...);
}

inline constexpr auto value(QGeoPositionInfo const &info, Attr attr) noexcept {
    switch(attr) {
    case Lat: return coord(info).latitude();
    case Lon: return coord(info).longitude();
    case Alt: return coord(info).altitude();
    default: return info.attribute((QGeoPositionInfo::Attribute)attr);
    }
}

inline constexpr auto get(QGeoPositionInfo const &info, Attr attr) noexcept {
    return has(info, attr)? value(info, attr) : defs::nothing;
}

inline constexpr void set(QGeoPositionInfo &info, auto attr, auto value) noexcept {
    if(defs::defined(value) || has(info, attr)) info.setAttribute((QGeoPositionInfo::Attribute)attr, value);
}

inline qint64 timestamp(QGeoPositionInfo const &info) {
    return info.timestamp().toMSecsSinceEpoch();
}

inline constexpr qreal hDist(auto const &pointA, auto const &pointB) noexcept {
    auto const &a = coord(pointA), &b = coord(pointB);
    return are2D(a, b)? a.distanceTo(b) : defs::nothing;
}

inline constexpr qreal vDist(auto const &pointA, auto const &pointB) noexcept {
    auto const &a = coord(pointA), &b = coord(pointB);
    return are3D(a, b)? std::fabs(a.altitude() - b.altitude()) : defs::nothing;
}

template<Geometry g>
inline constexpr qreal dist(auto const &pointA, auto const &pointB) noexcept {
    if constexpr(g == H) return hDist(pointA, pointB);
    else if constexpr(g == V) return vDist(pointA, pointB);
    else {
        auto const &a = coord(pointA), &b = coord(pointB);
        switch(type(a, b)) {
        case QGeoCoordinate::Coordinate3D: {
            auto h = hDist(a, b), v = vDist(a, b);
            return defs::norm(h, v);
        }
        case QGeoCoordinate::Coordinate2D: return hDist(a, b);
        default: return defs::nothing;
        }
    }
}

template<Geometry g = D3>
inline constexpr qreal speed(QGeoPositionInfo const &info) noexcept {
    if constexpr(g == D3) {
        auto h = speed<H>(info), v = speed<V>(info);
        return defs::defined(h) && defs::defined(v)? defs::norm(h, v) : h;
    }
    else return get(info, geometricSpd<g>);
}

template<Geometry g = D3>
inline constexpr qreal speed(QGeoPositionInfo const &info, QGeoPositionInfo const &last) noexcept {
    if(!are2D(info)) [[unlikely]] return defs::nothing;
    bool tooSoon{};
    if(isValid<g>(last)) {
        auto ds = dist<g>(last, info);
        if(defs::defined(ds)) [[likely]] {
            auto dt = timestamp(info) - timestamp(last);
            if(dt >= defs::tooSoon) [[likely]] return ds * 1000 / dt;
            else tooSoon = true;
        }
    }
    if constexpr(g == D3) {
        if(!tooSoon) [[likely]] {
            auto horizontal = speed<H>(info, last);
            if(defs::defined(horizontal)) [[likely]] return horizontal;
        }
    }
    return speed<g>(info);
}

template<Geometry g = D3>
inline constexpr qreal speed(QGeoPositionInfo const &before, QGeoPositionInfo const &now, QGeoPositionInfo const &after) noexcept {
    if(!isValid<g>(now)) [[unlikely]] {
        if constexpr(g != H) return speed<H>(before, now, after);
        else return defs::nothing;
    }
    qreal incoming, outgoing;
    if constexpr(g == V) {
        incoming = get(now, Alt) - get(before, Alt);
        outgoing = get(after, Alt) - get(now, Alt);
    }
    else {
        incoming = dist<g>(before, now);
        outgoing = dist<g>(now, after);
    }
    if(!defs::defined(incoming)) [[unlikely]] return speed<g>(after, now);
    if(!defs::defined(outgoing)) [[unlikely]] return speed<g>(now, before);
    auto dt = timestamp(after) - timestamp(before);
    if(dt < defs::tooSoon) [[unlikely]] return speed<g>(now);
    return (incoming + outgoing) * 1000 / dt;
}

inline constexpr auto dir(QGeoCoordinate const &) noexcept {
    return defs::nothing;
}

inline constexpr auto dir(QGeoPositionInfo const &info) noexcept {
    return get(info, Dir);
}

inline constexpr qreal dir(auto const &info, auto const &last) noexcept {
    auto const &now = coord(info), &before = coord(last);
    if(info.isValid() && last.isValid() && (lat(info) != lat(last) || lon(info) != lon(last))) [[likely]] {
        return before.azimuthTo(now);
    }
    return dir(info);
}

inline constexpr qreal dir(auto const &before, auto const &now, auto const &after) noexcept {
    qreal incoming = dir(before, now);
    qreal outgoing = dir(now, after);
    return defs::calm(defs::mid(incoming, defs::calm(outgoing, incoming)));
    if(defs::defined(incoming)) [[likely]] {
        if(defs::defined(outgoing)) [[likely]] {
            return 0.5 * (incoming + defs::calm(outgoing, incoming));
        }
        return incoming;
    }
    return outgoing;
}

inline QGeoCoordinate coordinate() noexcept { return {}; }
inline QGeoCoordinate coordinate(double lat, double lon) noexcept {
    if(defs::defined(lat, lon)) return {lat, lon};
    return {};
}
inline QGeoCoordinate coordinate(double lat, double lon, double alt) noexcept {
    if(defs::defined(lat, lon, alt)) return {lat, lon, alt};
    return coordinate(lat, lon);
}

inline QDateTime dateTime(QDateTime rv) { return rv; }
inline QDateTime dateTime(qint64 timestamp) { return QDateTime::fromMSecsSinceEpoch(timestamp); }

inline QGeoPositionInfo positionInfo(QGeoPositionInfo where, auto time) {
    where.setTimestamp(time);
    return where;
}
inline QGeoPositionInfo positionInfo(QGeoCoordinate const &coord, auto time) {
    return {coord, dateTime(time)};
}
inline QGeoPositionInfo positionInfo(QGeoCoordinate const &coord, auto time, Attr attr, auto value, auto... otherAttrs) {
    QGeoPositionInfo rv{positionInfo(coord, time, otherAttrs...)};
    set(rv, attr, value);
    return rv;
}
inline QGeoPositionInfo positionInfo(QGeoCoordinate const &coord, auto time, qreal spd) {
    return positionInfo(coord, time, HSpd, spd);
}

inline QGeoPositionInfo extrapolate(QGeoPositionInfo point, auto timepoint) noexcept {
    if(has(point, HSpd, Dir)) {
        auto hSpd = value(point, HSpd);
        auto dir = value(point, Dir);
        auto dt = timepoint - timestamp(point);
        if(dt < 0) {
            dt = -dt;
            dir = dir - 180;
        }
        auto crd = coord(point);
        if(has(point, Alt, VSpd)) {
            auto dh = get(point, VSpd) * dt / 1000;
            crd.setAltitude(crd.altitude() + dh);
        }
        point.setCoordinate(crd.atDistanceAndAzimuth(hSpd * dt / 1000, dir));
        return point;
    }
    return positionInfo(std::move(point), timepoint);
}

// Note: this function is added here to be used in filter.cpp under one very serious assumption:
// that is, under not very abnormal circumstances, there shouldn't be long lap/start/pause/milestone sequences between two neighbouring points.
// Another assumption is that valid infos will have their speeds set.
inline QGeoPositionInfo interpolate(QGeoPositionInfo const &start, QGeoPositionInfo const &stop, qreal ksi, qreal normal = {}) {
    auto t0 = timestamp(start), t1 = timestamp(stop);
    if(t0 >= t1) /*very*/ [[unlikely]] return start;
    auto dt = (t1 - t0) * ksi;
    auto timepoint = t0 + dt;
    auto const &a = coord(start), b = coord(stop);
    auto h0 = alt(a), h1 = alt(b);

    auto dist = ksi * a.distanceTo(b);
    auto azimuth = dir(a, b) + normal;
    qreal dh = defs::nothing;
    if(defs::defined(h1)) {
        if(defs::defined(h0)) dh = ksi * (h0 - h1);
        else {
            auto vv1 = speed<V>(stop);
            if(defs::defined(vv1)) dh = -vv1 * dt / 1000;
        }
    }
    if(!defs::defined(dh)) {
        auto vv0 = speed<V>(start);
        if(defs::defined(vv0)) dh = vv0 * dt / 1000;
    }
    if(!defs::defined(dh)) dh = 0;
    return positionInfo(a.atDistanceAndAzimuth(dist, azimuth, dh), timepoint);
}
}

#endif
