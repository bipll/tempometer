#include "trackreplay.h"
#include "files.h"
#include "defs.h"

TrackReplay::TrackReplay(QObject *parent)
    : QObject{parent} { qDebug() << "TRACK REPLAY: " << this; }

void TrackReplay::fastForward(quint32 laps) {
    qDebug() << "TRACK REPLAY: FF, this " << this << "replaying" << laps << "laps";
    for(quint32 i{1}, j{}; i <= laps; ++i) {
        qDebug() << "Replaying" << dir.absoluteFilePath(files::filename(i));
        lap.setFileName(dir.absoluteFilePath(files::filename(i)));
        if(lap.open(QIODevice::ReadOnly)) {
            if(std::decay_t<decltype(files::FILE_FORMAT)> format{}; read(format) && format == files::FILE_FORMAT) {
                if(qint64 timestamp; !read(timestamp)) continue;
                else {
                    qDebug() << "TrackNo:" << i;
                    emit trackNoChanged(i);
                    emit timestampChanged(timestamp);
                }
                files::DiskPoint<> point;
                while(read(point)) qDebug() << "Point @" << point.timestamp() << " #" << j++ << ": " << point.latitude() << point.longitude(), emit nextPoint(
                            point.timestamp(),
                            QGeoCoordinate(
                                point.latitude(),
                                point.longitude(),
                                point.elevation()
                                )
                            );
                lap.close();
            }
        }
    }
}

void TrackReplay::replayLap(quint32 lap, bool fast) {
}

void TrackReplay::forget(quint32 lap) {
    if(!lap) dir.removeRecursively();
    else dir.remove(files::filename(lap));
}
