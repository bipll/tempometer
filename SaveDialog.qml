import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15
import QtQuick.Window 2.15

import QtQuick.Layouts 1.3
import QtLocation 5.15
import QtPositioning 5.15
import Qt.labs.qmlmodels 1.0
import Qt.labs.platform 1.1
import 'Util.js' as U
import 'Format.js' as Fmt

Item {
    id: save

    signal saved
    signal canceled

    onSaved: clear()

    property var track: null
    onTrackChanged: {
        trackStats.model.rows = [
                    { key: 'start', value: getDate(track.timestamp) },
                    { key: 'finis', value: getDate(finishTime) },
                    { key: 'laps', value: laps.length },
                    { key: 'Nmost', value: Fmt.asLat(track.north, 'N', 'S') },
                    { key: 'Smost', value: Fmt.asLat(track.south, 'N', 'S') },
                    { key: 'Wmost', value: Fmt.asLon(track.west, 'E', 'W') },
                    { key: 'Emost', value: Fmt.asLon(track.east, 'E', 'W') },
                    { key: 'time', value: Fmt.time(track.time, false) },
                    { key: 'dist', value: Fmt.dist(track.dist, false) },
                    { key: 'avg', value: getAvg(track.dist, track.time) },
                    { key: 'mtime', value: Fmt.time(track.mtime, false) },
                    { key: 'mavg', value: getAvg(track.dist, track.mtime) }
                ]
        let types = getTypes(track)
        trackMetadata.types = types
        trackMetadata.type = types[0]
        console.log('Types:', trackMetadata.types)
        trackMetadata.comments = getComments(track)
        trackStrings.model.rows = [
                    { key: 'type', value: trackMetadata.types[0] },
                    { key: 'comment', value: trackMetadata.comments[0] }
                ]
    }

    property var laps: []
    property var curves

    /*
    property var viewPort: QtPositioning.rectangle(
                               QtPositioning.coordinate(total.north, total.west),
                               QtPositioning.coordinate(total.south, total.east)
                               )
                               */

    /*
    property string pluginName: 'osm'
    property list<PluginParameter> pluginParams: [
        PluginParameter { name: "osm.useragent"; value: "tempometer" }
        //PluginParameter { name: "osm.mapping.providersrepository.disabled"; value: "true" },
        //PluginParameter { name: "osm.mapping.providersrepository.address"; value: "http://maps-redirect.qt.io/osm/5.6/" }
    ]
    */

    property Plugin plugin: Plugin {
        name: main.pluginName
        parameters: main.pluginParams
    }

    Item {
        id: trackOverview
        width: parent.width
        x: 0
        y: 0
        height: 210

        Map {
            id: map
            x: 10
            y: 5
            width: 150
            height: 200
            //visibleRegion: viewPort

            plugin: save.plugin
            gesture.enabled: false
        }

        TableView {
            id: trackStats

            anchors.top: trackOverview.top
            anchors.left: map.right
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.bottom: trackOverview.bottom

            //columnWidthProvider: column => 32 + 200 * column
            model: TableModel {
                TableModelColumn { display: "key" }
                TableModelColumn { display: "value" }

                rows: []
            }

            delegate: Text {
                text: display
                font.pointSize: 14
                font.family: 'Droid Sans Mono'
            }

            columnSpacing: 10
            clip: true
        }
    }

    Item {
        id: trackMetadata
        anchors.left: parent.left
        //anchors.leftMargin: 10
        anchors.top: trackOverview.bottom
        //anchors.topMargin: 10
        anchors.right: parent.right
        anchors.margins: 5
        /*
        x: 10
        y: trackStats.y + trackStats.height + 10
        width: save.width - 20
        */
        height: 92
        clip: false

        property var types: []
        onTypesChanged: types.forEach(type => { otherTypes.model.append({'type': type}) })
        property var comments: []

        property string type
        property string comment

        TableView {
            id: trackStrings

            flickableDirection: Flickable.HorizontalFlick

            anchors.fill: parent

            model: TableModel {
                TableModelColumn { display: "key" }
                TableModelColumn { display: "value" }

                rows: []
            }

            rowHeightProvider: row => 36 + 20 * row
            columnWidthProvider: column => 68 + 252 * column

            function unfocus(row) {

            }

            delegate: DelegateChooser {
                DelegateChoice {
                    column: 1
                    row: 0
                    delegate: TextField {
                        id: type
                        text: display
                        onTextChanged: {
                            console.log('Text:', 't =', text, 'd =', displayText, 's =', selectedText, 'e =', preeditText)
                            trackMetadata.type = text
                            otherTypes.visible = false
                        }
                        onTextEdited: {
                            console.log('Edited:', 't =', text, 'd =', displayText, 's =', selectedText, 'e =', preeditText)
                        }

                        property bool shortPress: false
                        onPressed: { shortPress = true; event.accepted = true }
                        onPressAndHold: { shortPress = false; event.accepted = true }
                        onReleased: { if(shortPress) otherTypes.visible = !otherTypes.visible; event.accepted = true }
                        //onFocusChanged: if(!focus) otherTypes.visible = false

                        background: Rectangle {
                            width: 318
                            //implicitWidth: 200
                            //implicitHeight: 32
                            color: 'transparent'
                            border.color: '#21be2b'
                        }
                    }
                }
                DelegateChoice {
                    column: 1
                    row: 1
                    delegate: ScrollView {
                        TextArea {
                            id: comment
                            text: display
                            onTextChanged: trackMetadata.comment = text
                            font.pointSize: 14
                            font.family: 'Droid Sans Mono'
                            wrapMode: TextEdit.Wrap
                            height: 40
                            implicitWidth: 318
                            background: Rectangle {
                                width: 318
                                height: 52
                                color: 'transparent'
                                border.color: '#21be2b'
                            }
                        }
                    }
                }
                DelegateChoice {
                    delegate: Text {
                        text: display
                        font.pointSize: 14
                        font.family: 'Droid Sans Mono'
                    }
                }
            }

            columnSpacing: 5
            rowSpacing: 2
            clip: true
        }
    }

    RowLayout {
        id: checkBoxes
        anchors.top: trackMetadata.bottom
        anchors.topMargin: 5
        anchors.left: save.left
        anchors.right: save.right

        CheckBox {
            id: saveTrack
            checkable: true
            checked: true
            text: 'Save track'
        }
        CheckBox {
            id: saveLaps
            checkable: !saveTrack.checked
            checked: saveTrack.checked
            text: 'Save laps'
        }
    }

    ListView {
        id: otherTypes
        x: 120
        y: trackMetadata.y + 30
        z: 10
        width: contentItem.childrenRect.width
        height: visible * Math.min(21 * model.count, 120)
        clip: true
        Behavior on height { NumberAnimation { duration: 100 } }
        model: ListModel {}
        delegate: Text {
            text: type; font.pointSize: 21; z: 10
            TapHandler {
                onTapped: {
                    console.log('Tapped')
                    trackStrings.model.setData(trackStrings.model.index(0, 1), 'display', text)
                    otherTypes.visible = false
                }

                onCanceled: point.accepted = false
                onDoubleTapped: point.accepted = false
                onGrabChanged: point.accepted = false
                onSingleTapped: point.accepted = false
            }
        }
        visible: false
    }

    Rectangle {
        x: otherTypes.x - 5
        y: otherTypes.y - 3
        width: otherTypes.width + 10
        height: visible * Math.min(21 * otherTypes.model.count + 5, 140)
        Behavior on height { NumberAnimation { duration: 100 } }
        z: otherTypes.z - 1
        visible: otherTypes.visible
        color: '#F0F0F0'
        border.color: '#808080'
    }

    Button {
        id: button
        anchors.top: checkBoxes.bottom
        anchors.topMargin: 20
        anchors.horizontalCenter: save.horizontalCenter
        text: 'SAVE'
        onClicked: saveTracks()
    }

    Shape {
        id: divisor
        anchors.top: button.bottom
        anchors.topMargin: 10
        x: 0
        width: save.width

        ShapePath {
            strokeColor: 'grey'
            startX: 5
            startY: 0
            PathLine { x: width - 10; y: 0 }
        }
    }

    TableView {
        id: oldTracks
        anchors.top: divisor.bottom
        anchors.bottom: save.bottom
        anchors.left: save.left
        anchors.leftMargin: 10
        anchors.right: save.right
        anchors.rightMargin: 10

        model: TableModel {
            TableModelColumn { display: 'date' }
            TableModelColumn { display: 'dist' }
            TableModelColumn { display: 'time' }
            TableModelColumn { display: 'laps' }
            TableModelColumn { display: 'type' }    // it is a mere coincidence that all the column names are four-letter words

            rows: []
        }
        property var headline: ['date', 'dist', 'time', 'laps', 'type']

        delegate: Text { text: display }

        columnSpacing: 5
        rowSpacing: 5
        clip: true
    }

    MouseArea {
        anchors.fill: parent
        z: -10
        onClicked: otherTypes.visible = false
    }

    function inflate() {
        curves.map(function(curve, index) {
            console.log("Saving curve:", curve, curve.pathLength())
            for(let wat of curve.path) console.log("\tPoint:", wat)
            let copy = curveFactory.createObject(map, { path: curve.path })
            copy.line.color = U.lineColor(index + 1)
            copy.line.width = 4
            map.addMapItem(copy)
            //map.addMapItem(curve)
        })

        map.fitViewportToMapItems()
        oldTracks.model.rows = [oldTracks.headline.reduce((hdrs, key) => { hdrs[key] = key; return hdrs }, new Object())].concat(db.trackHeaders())

        //console.log('Tempometer from QML:', "Viewport:", save.viewPort)
        //console.log('Tempometer from QML:', "Curves:", curves.length, "; Map center:", map.center.latitude, map.center.longitude)
    }

    function hide() {
        map.clearMapItems()
    }

    function saveTracks() {
        let type = trackMetadata.type.trim()
        let comment = trackMetadata.comment.trim()

        let trackId = db.saveTrack(track, saveLaps.checked? laps : [], type, comment, saveTrack.checked)
        if(trackId !== '' && saveTrack.checked && saveLaps.checked) current.keep(trackId, 1, laps.length)
        saved()
        //flashOn()
    }

    function cancel() {
        canceled()
    }

    function getTypes(lap) {
        let rv = db.types()
        return rv.length === 0? [''] : rv
    }

    function getComments(lap) {
        let rv = db.comments()
        return rv.length === 0? [''] : rv
    }

    function getAvg(dist, time) { return Fmt.missing(dist) || Fmt.missing(time) || time < 5000? '---' : (dist * 3600 / time).toFixed(3) }

    function getDate(ms) {
        return new Date(ms).toISOString().replace(/\.\d*/, '')
    }

    function thumbname() {
        let locations = StandardPaths.standardLocations(StandardPaths.AppDataLocation)
        switch(locations.length) {
        case 0: return ''
        case 1: return locations[0].substring(7) + '/' + track.timestamp + '.png'
        default: return locations[1].substring(7) + '/' + track.timestamp + '.png'
        }
    }

    function clear() {
        map.clearMapItems()
        curves = []
        laps = []
    }
}
