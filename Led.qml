import QtQuick 2.15
import QtQuick.Layouts 1.3
import 'Format.js' as Fmt

Item {
    id: led
    property string label
    property Value value: Value {}  // it's value! property value!

    property real fontScale: 0.4
    property color textColor: 'black'

    Text {
        text: label
        anchors.top: parent.top
        anchors.left: parent.left
    }
    Text {
        anchors.centerIn: parent
        text: format(value)[0]
        fontSizeMode: Text.HorizontalFit
        font.pointSize: led.height * fontScale
        color: textColor
    }
    Text {
        text: format(value)[1]
        width: contentWidth
        height: contentHeight
        anchors.bottom: parent.bottom
        anchors.right: parent.right
    }

    property var format: Fmt.asSpeed
}
