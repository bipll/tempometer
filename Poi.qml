import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtLocation 5.15
import QtGraphicalEffects 1.15
import 'Util.js' as U

MapQuickItem {
    id: poi
    z: 1024
    property int planned: 0
    property string name: ''
    property string description: ''
    property string dbId: ''

    anchorPoint.x: width / 2
    anchorPoint.y: icon.height + 4

    property real scale: planned? 1 : -1
    Behavior on scale { NumberAnimation { duration: 400 } }

    //onPlannedChanged: flashDown.restart()

    sourceItem: ColumnLayout {
        id: srcItm
        spacing: 6
        z: 1024
        Image {
            id: icon
            Layout.alignment: Qt.AlignHCenter
            cache: false
            source: poi.scale > 0? 'images/poi.png' : 'images/greyPoi.png'
            scale: Math.abs(poi.scale)
        }
        Text {
            horizontalAlignment: Text.AlignHCenter
            text: name
            style: Text.Outline
            styleColor: U.outlineColor
            color: planned? 'green' : 'darkgrey'
        }
        MouseArea {
            Layout.fillWidth: true
            Layout.fillHeight: true
            onPressAndHold: {
                console.log('PressAndHold')
                if(main.route.length === 0 || main.route[main.route.length - 1] !== poi) (planned? addRemove : addOnly).popup()
                else removeOnly.popup()
            }
            onClicked: {
                console.log('clicked')
                if(planned) deroute()
                else enroute()
            }
        }
    }

    function setItems(menu, ...items) {
        for(let item of items) menu.addItem(item)
        menu.addItem(editRt)
        menu.addItem(navigate)
    }

    function enroute() { main.enroute(poi) }
    function deroute() { main.deroute(poi) }
    function toggleSave() {
        if(dbId) {
            db.forgetPoi(dbId)
            dbId = ''
        }
        else map.savePoi(poi)
    }

    Menu {
        id: addRemove
        MenuItem { text: 'Add to rt'; onTriggered: enroute() }
        MenuItem { text: 'Remove from rt'; onTriggered: deroute() }
        MenuItem { text: dbId? 'Unsave from lib' : 'Save to lib'; onTriggered: toggleSave() }
        MenuItem { text: 'Edit rt'; onTriggered: main.editRt() }
        MenuItem { text: 'Build track to'; onTriggered: main.buildTrack(poi) }
    }
    Menu {
        id: addOnly
        MenuItem { text: 'Add to rt'; onTriggered: enroute() }
        MenuItem { text: dbId? 'Unsave from lib' : 'Save to lib'; onTriggered: toggleSave() }
        MenuItem { text: 'Edit rt'; onTriggered: main.editRt() }
        MenuItem { text: 'Build track to'; onTriggered: main.buildTrack(poi) }
    }
    Menu {
        id: removeOnly
        MenuItem { text: 'Remove from rt'; onTriggered: deroute() }
        MenuItem { text: dbId? 'Unsave from lib' : 'Save to lib'; onTriggered: toggleSave() }
        MenuItem { text: 'Edit rt'; onTriggered: main.editRt() }
        MenuItem { text: 'Build track to'; onTriggered: main.buildTrack(poi) }
    }
    property MapCircle aura: MapCircle {
        backend: MapCircle.OpenGL
        opacity: 0.05
        z: -1024
        border.color: 'green'
        color: 'lightgreen'
        center: poi.coordinate
        radius: cfg.wptSensitivity
    }
    onPlannedChanged: if(planned) poiLayer.addOnce(aura); else poiLayer.remove(aura)
}
