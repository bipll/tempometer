#include <chrono>
#include <cmath>

#include <QGeoCoordinate>
#include <QGeoPositionInfoSource>
#include <QObject>
#include <QStandardPaths>
#include <QString>

#include "tracksegment.h"
#include "defs.h"
#include "geo.h"

namespace {
bool less(qreal next, qreal best) noexcept { return std::isnan(best) || next < best; }
bool more(qreal next, qreal best) noexcept { return std::isnan(best) || next > best; }
}

TrackSegment::TrackSegment(QCoreApplication *parent, quint32 startLap, qreal mile)
    : Super(parent), mile(mile), nextMilestone(mile), firstLap(startLap)
{
    pointCuboid(defs::nothing, defs::nothing, defs::nothing);
    setDist(defs::nothing);
    setTime(-1);
    setMtime(-1);
    setAvg(defs::nothing);
    setMavg(defs::nothing);
}

void TrackSegment::start() {
    setLapNo(firstLap);
    pressRec();
}

void TrackSegment::pointCuboid(qreal lat, qreal lon, qreal ele) {
    setNorth(lat);
    setSouth(lat);
    setWest(lon);
    setEast(lon);
    setTop   (ele);
    setBottom(ele);
}

void TrackSegment::resetStats() {
    auto head = coord();
    pointCuboid(head.latitude(), head.longitude(), head.altitude());
    setDist(0);
}

void TrackSegment::pressRec() {
    auto t0 = when();
    setTimestamp(t0);

    if(coord().isValid()) resetStats();
    else {
        pointCuboid(defs::nothing, defs::nothing, defs::nothing);
        setDist(defs::nothing);
    }

    setTime(0);
    setMtime(0);
    setAvg(defs::nothing);
    setMavg(defs::nothing);
    unpause();
}

void TrackSegment::pause() {
    setRecording(false);
}

void TrackSegment::update(QGeoPositionInfo const &info) {
    auto thisPoint = coord(info);
    bool knownWhereabouts = thisPoint.isValid();
    setFix(knownWhereabouts);

    if(recording()) {
        auto lastPoint = coord(lastKnownWhereabouts);
        auto lastKnownAt = coord(track());
        if(knownWhereabouts) {
            if(lastPoint.isValid()) {
                auto stride = lastPoint.distanceTo(thisPoint);
                auto nextDist = dist() + stride;
                bool checkpointReached = stride > 0 && nextDist >= nextMilestone;

                if(checkpointReached) {
                    while(checkpointReached) {
                        auto ksi = (nextMilestone - dist()) / stride;
                        auto milestone = geo::interpolate(track(), info, ksi, normal);
                        setMilestoneAchieved(nextMilestone);
                        setCheckpoint(milestone);
                        nextMilestone += mile;
                        checkpointReached = nextDist >= nextMilestone;
                    }
                    setTrack(info);
                }
                setDist(nextDist);
            }

            auto latitude = thisPoint.latitude();
            if(more(latitude, north())) setNorth(latitude);
            if(less(latitude, south())) setSouth(latitude);

            auto longitude = thisPoint.longitude();
            if(more(longitude, east())) setEast(longitude);
            if(less(longitude, west())) setWest(longitude);

            auto altitude = thisPoint.altitude();
            if(more(altitude, top   ())) setTop   (altitude);
            if(less(altitude, bottom())) setBottom(altitude);

            trim();
        }
        auto dt = geo::timestamp(info) - geo::timestamp(where());
        setTime(time() + dt);
        if(time() >= 5000) setAvg(dist() * 1000 / time());
        if(geo::speed(info) >= defs::tooSlow) setMtime(mtime() + dt);
        if(mtime() >= 5000) setMavg(dist() * 1000 / mtime());
    }
    setWhere(info);
    if(knownWhereabouts) lastKnownWhereabouts = info;
}

void TrackSegment::unpause() {
    setRecording(true);
    setTrack(where());
}

void TrackSegment::endLap() {
    if(track() != where()) setTrack(where());
}

void TrackSegment::lap() {
    auto no = lapNo() + 1;
    setLapNo(no);
    pressRec();
}

void FilteredTrack::trim() {
    if(ripened()) setTrack(where());
}

bool FilteredTrack::ripened() const {
    auto const &tr = track();
    if(!tr.isValid()) return true;

    static constexpr auto delta = 0.4;

    auto wh = where();
    auto thisCoord = wh.coordinate(), lastCoord = tr.coordinate();
    auto howFar = lastCoord.distanceTo(thisCoord);
    if(howFar < delta) return false;
    if(howFar >= 5 * spd()) return true;

    static constexpr qreal factor = 180 / 3.14159265358979323846;
    static constexpr qreal corridorWidth = delta * factor;

    qreal searchLight = lastCoord.azimuthTo(thisCoord);
    qreal heading = geo::dir(tr);
    qreal yaw = defs::calm(searchLight - heading);
    if(yaw >= 1) {
        normal = -90;
        if(yaw >= 12) return true;
    }
    else if(yaw <= -1) {
        normal = 90;
        if(yaw <= -12) return true;
    }
    return corridorWidth <= yaw * howFar;
}
