.pragma library

.import 'Format.js' as Fmt

function isString(val) {
    return typeof val === 'string' || val instanceof String
}

function toDateString(timestamp, seconds = false) {
    let date = new Date(timestamp * 1000)
    let tail = seconds? `:${date.getSeconds().toString().padStart(2, '0')}.${date.getMilliseconds().toString().padStart(3, '0')}` : ''
    return `${date.getFullYear() % 100}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${(date.getDate()).toString().padStart(2, '0')} ` +
            `${(date.getHours()).toString().padStart(2, '0')}:${(date.getMinutes()).toString().padStart(2, '0')}${tail}` // v_v
}

function getAvg(dist, time) { return Fmt.missing(dist) || Fmt.missing(time) || time < 5000? '---' : (dist * 3600 / time).toFixed(3) }

function getStr(str, padding) { return str === null? ' '.repeat(padding) : str.padEnd(padding, ' ') }

function getTime(time) { return Fmt.time(time, false) + `.${(time % 1000 | 0).toString().padStart(3, '0')}` }

const createEverything = [
                       'CREATE TABLE IF NOT EXISTS Exercises(exercise VARCHAR(64) NOT NULL UNIQUE)',
                       'CREATE INDEX IF NOT EXISTS exerciseNames ON Exercises(exercise)',

                       'CREATE TABLE IF NOT EXISTS Types(' +
                            'type VARCHAR(64) NOT NULL UNIQUE, ' +
                            'exercises INTEGER, ' +
                            'recent BOOLEAN DEFAULT TRUE, ' +
                            'used INT DEFAULT 0' +
                       ')',
                       'CREATE INDEX IF NOT EXISTS typeNames ON Types(type)',
                       'CREATE INDEX IF NOT EXISTS recentType ON Types(recent)',

                       'CREATE TABLE IF NOT EXISTS Comments(' +
                            'comment TEXT UNIQUE, ' +
                            'recent BOOLEAN DEFAULT TRUE, ' +
                            'used_with ROWID, ' +
                            'FOREIGN KEY (used_with) REFERENCES Types(rowid)' +
                       ')',
                       'CREATE INDEX IF NOT EXISTS recentComment ON Comments(recent)',

                       'CREATE TABLE IF NOT EXISTS Tracks(' +
                       // Please don't use this app to start recording two tracks at the same millisecond
                            'timestamp TIMESTAMP(3) NOT NULL PRIMARY KEY, ' +
                            'laps INT UNSIGNED NOT NULL CHECK(laps >= 1), ' +
                            'north DOUBLE, ' +
                            'south DOUBLE, ' +
                            'east DOUBLE, ' +
                            'west DOUBLE, ' +
                            'saved BOOLEAN' +
                       ') WITHOUT ROWID',

                       'CREATE TABLE IF NOT EXISTS Laps(' +
                            'timestamp TIMESTAMP(3), ' +
                            'trackStart TIMESTAMP(3), ' +
                            'lapNo INT UNSIGNED NOT NULL, ' +
                            'time DOUBLE NOT NULL CHECK(time >= 0), ' +
                            'dist DOUBLE CHECK(dist >= 0), ' +
                            'mtime DOUBLE CHECK(mtime >= 0), ' +
                            'type INT UNSIGNED NULLABLE, ' +
                            'comment INT UNSIGNED NULLABLE, ' +
                            'PRIMARY KEY (trackStart, lapNo), ' +
                            'FOREIGN KEY (trackStart) REFERENCES Tracks(timestamp), ' +
                            'FOREIGN KEY (type) REFERENCES Types(id), ' +
                            'FOREIGN KEY (comment) REFERENCES Comments(id)' +
                       ') WITHOUT ROWID',

                       'CREATE TABLE IF NOT EXISTS Pois(' +
                            'lat DOUBLE NOT NULL, ' +
                            'lon DOUBLE NOT NULL, ' +
                            'name VARCHAR(64), ' +
                            'description TEXT' +
                       ')',

                       'CREATE TABLE IF NOT EXISTS Settings(' +
                            'defaultFor INT UNSIGNED NULLABLE UNIQUE, ' +
                            'mile DOUBLE NOT NULL DEFAULT 1000, ' +
                            'yard DOUBLE NOT NULL DEFAULT 1, ' +
                            'speedFactor DOUBLE NOT NULL DEFAULT 3.6, ' +
                            'wptSensitivity DOUBLE NOT NULL DEFAULT 20, ' +
                            'addNewWpts BOOLEAN NOT NULL DEFAULT TRUE, ' +
                            'startLapsOnWpts BOOLEAN NOT NULL DEFAULT FALSE' +
                       ')',
                       'CREATE INDEX IF NOT EXISTS typedSettings ON Settings(defaultFor)'
                   ]

function predefinedTypes(tx) {
    const exerciseNames = [
        'walk',
        'run',
        'ride',
        'skate',
        'fly',
        'swim',
        'dog',
        'bike',
        'ski',
        'board',
        'horse',
        'summer',
        'winter',
        'fast',
        'harsh',
        'plains',
        'mountains',
        'motor'
    ]
    let ex = {}
    let exId = 1
    for(let exName of exerciseNames) {
        ex[exName] = exId
        exId *= 2
    }

    for(let predefined of exerciseNames) {
        tx.executeSql('INSERT OR IGNORE INTO Exercises(rowid, exercise) VALUES(?, ?)', [ex[predefined], predefined])
    }

    for(let predefined of [
            ['bike', ex.ride | ex.bike | ex.summer | ex.plains],
            ['car', ex.motor],
            ['cycle', ex.ride | ex.bike | ex.fast | ex.summer | ex.plains],
            ['dog footbike', ex.ride | ex.bike | ex.dog | ex.summer | ex.plains],
            ['dog run', ex.run | ex.dog | ex.plains],
            ['dog walk', ex.walk | ex.dog | ex.plains],
            ['footbike', ex.ride | ex.bike | ex.summer | ex.plains],
            ['freeskate', ex.skate | ex.fast | ex.summer | ex.plains],
            ['glider', ex.fly],
            ['hike', ex.walk],
            ['ice skate', ex.skate | ex.fast | ex.winter | ex.plains],
            ['inline skate', ex.skate | ex.fast | ex.summer | ex.plains],
            ['longboard', ex.board | ex.fast | ex.summer | ex.plains],
            ['miniboard', ex.board | ex.summer | ex.plains],
            ['motorcycle', ex.motor],
            ['mountain bike', ex.ride | ex.bike | ex.harsh | ex.mountains | ex.summer],
            ['mountain ski', ex.ski | ex.winter | ex.mountains],
            ['plane', ex.motor | ex.fly],
            ['quad skate', ex.skate | ex.summer | ex.plains],
            ['run', ex.run | ex.plains],
            ['skateboard', ex.board | ex.summer | ex.plains],
            ['ski', ex.ski | ex.winter | ex.plains],
            ['snowboard', ex.board | ex.winter | ex.mountains],
            ['swim', ex.swim],
            ['walk', ex.walk | ex.plains],
        ])
    {
        tx.executeSql('INSERT OR IGNORE INTO Types(type, exercises, recent) VALUES(?, ?, false)', predefined)
    }
}

class Db {
    constructor(storage) {
        this.db = storage.openDatabaseSync(
            "Tempometer",
            "1.0",
            "Tracks storage and other stuff",
            10247680)
        // DELETEME
        if(0) {
            this.db.transaction(function(tx) {
                tx.executeSql('DROP TABLE IF EXISTS Laps')
                tx.executeSql('DROP TABLE IF EXISTS Tracks')

                tx.executeSql('DROP INDEX IF EXISTS recentComment')
                tx.executeSql('DROP TABLE IF EXISTS Comments')

                tx.executeSql('DROP INDEX IF EXISTS recentType')
                tx.executeSql('DROP INDEX IF EXISTS typeNames')
                tx.executeSql('DROP TABLE IF EXISTS Types')

                tx.executeSql('DROP INDEX IF EXISTS exerciseNames')
                tx.executeSql('DROP TABLE IF EXISTS Exercises')

                tx.executeSql('DROP TABLE IF EXISTS Pois')
                tx.executeSql('DROP TABLE IF EXISTS Settings')
            })
        }
        // /DELETEME
        this.db.transaction(function(tx) {
            for(let query of createEverything) tx.executeSql(query)

            predefinedTypes(tx)
            if(!tx.executeSql('SELECT * FROM Settings').rows.length) tx.executeSql('INSERT INTO Settings DEFAULT VALUES')
        })
    }

    saveTrack(total, laps, type, comment, saved) {
        let rv = ''
        this.db.transaction(function(tx) {
            let trackKey = total.timestamp / 1000
            rv = tx.executeSql('INSERT INTO Tracks VALUES(?, ?, ?, ?, ?, ?, ?)',
                               [
                                   trackKey,
                                   Math.max(laps.length, 1),
                                   total.north,
                                   total.south,
                                   total.east,
                                   total.west,
                                   saved
                               ])
            rv = trackKey.toFixed(3)
            console.log('Track ID:', rv)

            let packagedReq = {
                statement: 'INSERT INTO Laps(timestamp, trackStart, lapNo, time',
                qmarks: '?, ?, ?, ?',
                stats: [trackKey, trackKey, 0, total.time],
                insertStatement: function() { return this.statement + ') VALUES (' + this.qmarks + ')' }
            }

            function addStat(key, value) {
                if(!isNaN(value)) {
                    packagedReq.statement += ', ' + key
                    packagedReq.qmarks += ', ?'
                    packagedReq.stats.push(value)
                }
            }
            addStat('dist', total.dist)
            addStat('mtime', total.mtime)

            function selectExisting(table, val, key) {
                if(isString(val) && val !== '') {
                    console.log('SELECT rowid FROM ' + table + ' where ' + key + ' = \'' + val + '\'')
                    let knownVal = tx.executeSql(`SELECT rowid FROM ${table} WHERE ${key} = \'${val}\'`) //, [table, key, val])
                    if(knownVal.rows.length >= 1) val = knownVal.rows.item(0).rowid
                    else {
                        knownVal = tx.executeSql(`INSERT INTO ${table} (${key}) VALUES(\'${val}\')`) //, [table, key, val])
                        val = parseInt(knownVal.insertId)
                    }
                }

                console.log(table, key, '=', val, Number.isInteger(val))
                if(Number.isInteger(val)) {
                    packagedReq.statement += ', ' + key
                    packagedReq.qmarks += ', ?'
                    packagedReq.stats.push(val)
                    return val
                }
            }
            let typeId = selectExisting('Types', type, 'type')
            console.log('TypeId returned:', typeId)
            if(typeId !== undefined) {
                tx.executeSql('UPDATE Types SET recent = false WHERE recent = true')
                tx.executeSql('UPDATE Types SET used = used + 1, recent = true WHERE rowid = ?', [typeId])
            }

            let commentId = selectExisting('Comments', comment, 'comment')
            if(commentId !== undefined) {
                tx.executeSql('UPDATE Comments SET recent = false WHERE recent = true')
                tx.executeSql('UPDATE Comments SET used_with = ?, recent = true WHERE rowid = ?', [typeId, commentId])
            }

            console.log(packagedReq.insertStatement(), packagedReq.stats)
            tx.executeSql(packagedReq.insertStatement(), packagedReq.stats)

            console.log('Laps:', laps)
            laps.map(function(lap, index) {
                console.log('i:', index, 'Lap:', lap, 'TS:', lap.timestamp)
                packagedReq.statement = 'INSERT INTO Laps(timestamp, trackStart, lapNo, time'
                packagedReq.qmarks = '?, ?, ?, ?'
                packagedReq.stats = [lap.timestamp / 1000, trackKey, index + 1, lap.time]
                addStat('dist', lap.dist)
                addStat('mtime', lap.mtime)
                console.log(packagedReq.insertStatement(), packagedReq.stats)
                tx.executeSql(packagedReq.insertStatement(), packagedReq.stats)
            })
        })
        return rv
    }

    select(query, f, params) {
        console.log('Query:', query)
        let rv = []
        if(f == undefined) f = Object.create
        this.db.readTransaction(function(tx) {
            let result = tx.executeSql('SELECT ' + query, params ?? [])
            for(let i = 0; i < result.rows.length; ++i) rv.push(f(result.rows.item(i)))
            return rv
        })
        return rv
    }

    trackHeaders(id = null) {
        console.log('Retrieving track headers after', id)
        let filter = ''
        let params = null
        if(id !== null) {
            filter = 'AND Tracks.timestamp > ? '
            params = [id]
        }

        return this.select('Tracks.*, Laps.time, Laps.dist, Laps.mtime, Types.type, Comments.comment ' +
                           'FROM Tracks ' +
                           'LEFT JOIN Laps on Laps.trackStart = Tracks.timestamp ' +
                           'LEFT JOIN Types on Types.rowid = Laps.type ' +
                           'LEFT JOIN Comments on Comments.rowid = Laps.comment ' +
                           'WHERE Laps.lapNo = 0 ' + filter +
                           'ORDER BY Tracks.timestamp DESC',
                           row => ({
                                       id: row.timestamp,
                                       date: toDateString(row.timestamp),
                                       laps: row.laps.toString(),
                                       lap: '',
                                       north: Fmt.asLat(row.north),
                                       south: Fmt.asLat(row.south),
                                       east: Fmt.asLon(row.east),
                                       west: Fmt.asLon(row.west),
                                       time: Fmt.time(row.time, false),
                                       dist: Fmt.dist(row.dist),
                                       avg: getAvg(row.dist, row.time),
                                       mtime: Fmt.time(row.mtime, false),
                                       mavg: getAvg(row.dist, row.time),
                                       type: getStr(row.type, 4),
                                       comment: getStr(row.comment, 7)
                                   }),
                           params)
    }

    types() {
        return this.select('type FROM Types ORDER BY recent DESC, used DESC, type ASC', row => row.type)
    }

    comments() {
        return this.select('comment FROM Comments ORDER BY recent DESC, rowid DESC', row => row.comment)
    }

    tracks(id = null) {
        console.log('Retrieving tracks after', id)
        let self = this
        return this.trackHeaders(id).map(function(header) {
            let laps = self.select('* FROM Laps WHERE trackStart = ? AND lapNo > 0 ORDER BY lapNo',
                                   row => ({
                                               id: header.id,
                                               date: toDateString(row.timestamp, true),
                                               laps: ' ',
                                               lap: row.lapNo.toString().padEnd(3, ' '),
                                               time: getTime(row.time),
                                               dist: Fmt.dist(row.dist),
                                               avg: getAvg(row.dist, row.time),
                                               mtime: getTime(row.mtime),
                                               mavg: getAvg(row.dist, row.time),
                                               type: header.type,
                                               comment: header.comment
                                           }),
                                   [header.id])
            return {header: header, laps: laps}
        })
    }

    forget(track, lap) {
        this.db.transaction(function(tx) {
            if(lap) tx.executeSql('DELETE FROM Laps WHERE trackstart = ? and lapNo = ?', [track, lap])
            else {
                tx.executeSql('DELETE FROM Laps WHERE trackstart = ?', [track])
                tx.executeSql('DELETE FROM Tracks WHERE timestamp = ?', [track])
            }
        })
    }

    pois(db, poiCtor) {
        return this.select('rowid, * FROM Pois', poiCtor)
    }

    addPoi(...args) {
        let rv = ''
        this.db.transaction(function(tx) {
            rv = tx.executeSql('INSERT INTO Pois(lat, lon, name, description) VALUES(?, ?, ?, ?)', args).insertId
        })
        return rv
    }

    forgetPoi(id) {
        this.db.transaction((tx) => tx.executeSql('DELETE FROM Pois WHERE rowid = ?', [id]))
    }

    currentSettings() {
        return this.select('* FROM Settings WHERE rowid = 1',
                           (row) => { return {
                                   mile: Number.parseFloat(row.mile),
                                   yard: Number.parseFloat(row.yard),
                                   speedFactor: Number.parseFloat(row.speedFactor),
                                   wptSensitivity: Number.parseFloat(row.wptSensitivity),
                                   addNewWpts: Number.parseInt(row.addNewWpts),
                                   startLapsOnWpts: Number.parseInt(row.startLapsOnWpts)
                               }})[0]
    }

    saveSettings(settings, defaultFor = null) {
        this.db.transaction(function(tx) {
            tx.executeSql('UPDATE Settings SET ' +
                          'defaultFor = ?, ' +
                          'mile = ?, ' +
                          'yard = ?, ' +
                          'speedFactor = ?, ' +
                          'wptSensitivity = ?, ' +
                          'addNewWpts = ?, ' +
                          'startLapsOnWpts = ? ' +
                          'WHERE rowid = 1',
                          [defaultFor,
                           settings.mile,
                           settings.yard,
                           settings.speedFactor,
                           settings.wptSensitivity,
                           settings.addNewWpts,
                           settings.startLapsOnWpts
                          ]
                          )
        })
    }
}
