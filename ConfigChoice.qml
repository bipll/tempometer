import QtQuick 2.15
import QtQuick.Controls 2.15
import 'Format.js' as Fmt

ConfigValue {
    id: configChoice
    property var choices
    property int index
    property int systemIndex
    property real choiceX: 70
    required property var lookup

    onSystemIndexChanged: { console.log('SystemIndex: ', systemIndex); choice.currentIndex = systemIndex }

    function setIndex(value) {
        let setting = lookup(value)
        console.log("Setting index, value:", value instanceof Number? 'Number' : value instanceof String? 'String' : typeof value, value, 'setting:', setting, 'choices:', choices)
        if(setting === undefined) choice.editText = Fmt.toString(value)
        else choice.currentIndex = setting
    }

    signal activated(int index)
    signal custom(real value)

    onActivated: console.log('ConfigChoice::activated', index)
    onCustom: console.log('ConfigChoice::custom', value)

    ComboBox {
        id: choice
        x: choiceX
        onCurrentIndexChanged: if(currentIndex < choices.length) configChoice.activated(currentIndex)
        editable: currentIndex === choices.length
        model: [...choices, 'custom']

        onEditTextChanged: parseEdit()

        function parseEdit() {
            if(editText === 'custom') editText = ''
            if(!editable || editText === '') return

            let rv = Number.parseFloat(editText)
            if(!isNaN(rv)) {
                let index = lookup(rv)
                if(index === undefined) configChoice.custom(rv)
                else {
                    currentIndex = index
                    configChoice.activated(index)
                }
            }
        }

        onActivated: configChoice.activated(index)
        onCurrentTextChanged: if(currentText === 'custom') currentText = ''
        /*
        onAcceptableInputChanged: console.log("COMBO_BOX acceptableInput\t:", acceptableInput, currentText, editText)
        onCountChanged: console.log("COMBO_BOX count\t:", count, currentText, editText)
        onCurrentIndexChanged: console.log("COMBO_BOX currentIndex\t:", currentIndex, currentText, editText)
        onCurrentValueChanged: console.log("COMBO_BOX currentValue\t:", currentValue, currentText, editText)
        onDelegateChanged: console.log("COMBO_BOX delegate\t:", delegate, currentText, editText)
        onDisplayTextChanged: console.log("COMBO_BOX displayText\t:", displayText, currentText, editText)
        onDownChanged: console.log("COMBO_BOX down\t:", down, currentText, editText)
        onEditTextChanged: console.log("COMBO_BOX editText\t:", editText, currentText, editText)
        onEditableChanged: console.log("COMBO_BOX editable\t:", editable, currentText, editText)
        onFlatChanged: console.log("COMBO_BOX flat\t:", flat, currentText, editText)
        onHighlightedIndexChanged: console.log("COMBO_BOX highlightedIndex\t:", highlightedIndex, currentText, editText)
        onImplicitIndicatorHeightChanged: console.log("COMBO_BOX implicitIndicatorHeight\t:", implicitIndicatorHeight, currentText, editText)
        onImplicitIndicatorWidthChanged: console.log("COMBO_BOX implicitIndicatorWidth\t:", implicitIndicatorWidth, currentText, editText)
        onIndicatorChanged: console.log("COMBO_BOX indicator\t:", indicator, currentText, editText)
        onInputMethodComposingChanged: console.log("COMBO_BOX inputMethodComposing\t:", inputMethodComposing, currentText, editText)
        onInputMethodHintsChanged: console.log("COMBO_BOX inputMethodHints\t:", inputMethodHints, currentText, editText)
        onModelChanged: console.log("COMBO_BOX model\t:", model, currentText, editText)
        onPopupChanged: console.log("COMBO_BOX popup\t:", popup, currentText, editText)
        onPressedChanged: console.log("COMBO_BOX pressed\t:", pressed, currentText, editText)
        onSelectTextByMouseChanged: console.log("COMBO_BOX selectTextByMouse\t:", selectTextByMouse, currentText, editText)
        onTextRoleChanged: console.log("COMBO_BOX textRole\t:", textRole, currentText, editText)
        onValidatorChanged: console.log("COMBO_BOX validator\t:", validator, currentText, editText)
        onValueRoleChanged: console.log("COMBO_BOX valueRole\t:", valueRole, currentText, editText)

        onAccepted: console.log("COMBO_BOX accepted\t:", currentText, editText)
        onActivated: console.log("COMBO_BOX activated\t:", currentText, editText)
        onHighlighted: console.log("COMBO_BOX highlighted\t:", currentText, editText)
        */
    }
}
