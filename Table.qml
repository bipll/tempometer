import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0

import 'Util.js' as U

Item {
    id: table
    anchors.fill: parent

    property var headline
    property TableModel headerModel
    property TableModel bodyModel

    property real fontSize: 14
    property real columnSpacing: 5
    property real rowSpacing: 10
    property real headerHeight: 18
    property var columnWidths

    property var data
    property var primaryRow: (datum) => datum
    property var secondaryRows: (datum) => []

    TableView {
        id: header
        anchors.top: parent.top
        width: parent.width
        height: table.headerHeight

        model: table.headerModel
        syncView: body
        syncDirection: Qt.Horizontal
        rowSpacing: 1
        columnSpacing: table.columnSpacing

        delegate: Text { text: display; font.pointSize: table.fontSize }
    }

    TableView {
        id: body
        anchors.top: header.bottom
        anchors.bottom: parent.bottom
        width: parent.width
        height: parent.height - table.headerHeight

        model: table.bodyModel
        rowSpacing: table.rowSpacing
        columnSpacing: table.columnSpacing

        columnWidthProvider: (col) => columnWidths[col]
        delegate: Text { text: display; font.pointSize: table.fontSize }
    }

    onDataChanged: {
        columnWidths = headline.map((columnHeader) => U.textWidth(columnHeader))
        header.model.rows = [headline.reduce((headers, columnHeader) => { headers[columnHeader] = columnHeader; return headers }, {})]
        if(data !== null) {
            body.model.rows = data.map(function(datum) {
                let values = primaryRow(datum)
                headline.forEach(function(columnHeader, column) {
                    let width = U.textWidth(values[columnHeader])
                    if(width > columnWidths[column]) columnWidths[column] = width
                })
                return values
            })
        }
        else body.model.rows = []
        console.log('widths:', columnWidths)
        console.log('rows:', body.model.rows.slice(0, 3))
        for(let i = 0; i < 2; ++i) for(let key in body.model.rows[i]) console.log(i, ':', key, '->', body.model.rows[i][key])
        console.log('width:', body.width, header.width, table.width)
        console.log('height:', body.height, header.height, table.height)
        body.forceLayout()
    }
}
