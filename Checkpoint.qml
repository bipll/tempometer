import QtQuick 2.0
import QtQuick.Shapes 1.15
import QtLocation 5.15
import 'Util.js' as U

MapQuickItem {
    id: checkpoint
    required property string text
    required property string otherText
    required property real direction
    property real mapBearing: 0
    property real beta: (direction + mapBearing) * Math.PI / 180
    required property color color
    property real cos: -Math.cos(beta)
    property real sin: Math.sin(beta)
    clip: false

    sourceItem: Item {
        Shape {
            ShapePath {
                strokeColor: checkpoint.color
                strokeWidth: 3
                startX: 5 * sin
                startY: 5 * cos
                PathLine {
                    x: -5 * sin
                    y: -5 * cos
                }
            }
        }

        Text {
            id: mainText
            color: checkpoint.color
            text: checkpoint.text
            property real halfWidth: contentWidth / 2
            property real halfHeight: contentHeight / 2
            property real dr: Math.max(halfWidth, halfHeight)
            x: (12 + dr) * sin - halfWidth
            y: (12 + dr) * cos - halfHeight
            style: Text.Outline
            styleColor: U.outlineColor
        }
        Text {
            color: checkpoint.color
            text: checkpoint.otherText
            property real halfWidth: contentWidth / 2
            property real halfHeight: contentHeight / 2
            property real dr: Math.max(halfWidth, halfHeight)
            x: (-12 - dr) * sin - halfWidth
            y: (-12 - dr) * cos - halfHeight
            style: Text.Outline
            styleColor: U.outlineColor
        }
    }
}
