import QtQuick 2.15
import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property string label
    property real labelOffset: 0
    Label { y: labelOffset; text: label; x: 30; padding: 5; background: Rectangle { anchors.fill: parent; radius: 2; color: '#F6F6F6' } }
}
