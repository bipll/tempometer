#include "median.h"

#include <3rdparty/quadrant.cpp/tests/test.h>

#include <iostream>
#include <tuple>
#include <type_traits>
#include <utility>

TEST(test_median) {
    smp::Median<int, int> m;
    m.push(10, 1000);           // [10]
    ASSERT_EQ(std::tuple{10, 1000}, m.value());
    m.push(11, 1100);
    m.push(12, 1200);           // 10 [11] 12
    ASSERT_EQ(std::tuple{11, 1100}, m.value());
    m.push(8, 1300);
    m.push(9, 1400);             // [10] 11 12 8 9
    ASSERT_EQ(std::tuple{10, 1200}, m.value());
    m.push(11, 1500);
    m.push(8, 1600);                  // [10] 11 12 8 9 11 8
    ASSERT_EQ(std::tuple{10, 1300}, m.value());

    int id{};
    for(auto [v, a, expv, expa]: std::vector<std::tuple<int, int, int, int>>{
            {1  , 1700, 9 , 1400},  // 11 12 8 [9] 11 8 1
            {2  , 1800, 8 , 1500},  // 12 8 9 11 [8] 1 2
            {10 , 1900, 8 , 1600},  // 8 9 11 [8] 1 2 10
            {24 , 2000, 9 , 1700},  // [9] 11 8 1 2 10 24
            {5  , 2100, 8 , 1800},  // 11 [8] 1 2 10 24 5
            {6  , 2200, 6 , 1900},  // 8 1 2 10 24 5 [6]
            {12 , 2300, 6 , 2000},  // 1 2 10 24 5 [6] 12
            {13 , 2400, 10, 2100},  // 2 [10] 24 5 6 12 13
            {14 , 2500, 12, 2200},  // 10 24 5 6 [12] 13 14
            {-1 , 2600, 12, 2300},  // 24 5 6 [12] 13 14 -1
            {128, 2700, 12, 2400},  // 5 6 [12] 13 14 -1 128
            {17 , 2800, 13, 2500},  // 6 12 [13] 14 -1 128 17
            {11 , 2900, 13, 2600},  // 12 [13] 14 -1 128 17 11
            {18 , 3000, 14, 2700},  // 13 [14] -1 128 17 11 18
            {19 , 3100, 17, 2800},  // 14 -1 128 [17] 11 18 19
            {32 , 3200, 18, 2900},  // -1 128 17 11 [18] 19 32
            {21 , 3300, 19, 3000},  // 128 17 11 18 [19] 32 21
            {22 , 3400, 19, 3100},  // 17 11 18 [19] 32 21 22
            {23 , 3500, 21, 3200},  // 11 18 19 32 [21] 22 23
            {24 , 3600, 22, 3300}   // 18 19 32 21 [22] 23 24
            })
    {
        m.push(v, a);
        ASSERT_EQ_AT(id++, std::tuple{expv, expa}, m.value());
    }

    // popping a single value (i.e. leaving the filter in an even-numbered state) is not a real prod situation
    for(auto [expv, expa]: std::vector<std::tuple<int, int>>{
                {22, 3400},
                {23, 3400},
                {22, 3500},
                {23, 3500}
            })
    {
        m.pop();
        ASSERT_EQ(std::tuple{expv, expa}, m.value());
    }
    // 22 [23] 24

    id = 0;
    for(auto [v, a, expv, expa]: std::vector<std::tuple<int, int, int, int>>{
            {2  , 3700, 22, 3600},  // [22] 23 24 2
            {25 , 3800, 23, 3600},  // [23] 24 25 2 25
            {0  , 3900, 22, 3700},  // [22] 23 24 2 25 0
            {128, 4000, 23, 3700},  // 22 [23] 24 2 25 0 128
            {23 , 4100, 23, 3800},  // [23] 24 2 25 0 128 23
            {49 , 4200, 24, 3900},  // [24] 2 25 0 128 23 49
            {16 , 4300, 23, 4000},  // 2 25 0 128 [23] 49 16
            {42 , 4400, 25, 4100},  // [25] 0 128 23 49 16 42
            {25 , 4500, 25, 4200},  // 0 128 23 49 16 42 [25]
            {23 , 4600, 25, 4300},  // 128 23 49 16 42 [25] 23
            {47 , 4700, 25, 4400},  // 23 49 16 42 [25] 23 47
            {40 , 4800, 40, 4500},  // 49 16 42 25 23 47 [40]
            {49 , 4900, 40, 4600}   // 16 42 25 23 47 [40] 49
            })
    {
        m.push(v, a);
        ASSERT_EQ_AT(id++, std::tuple{expv, expa}, m.value());

    }
};

TEST(multidim) {
    smp::Median<std::tuple<int, int, int>, int> m;
    m.push(1, 2, 3, 100);
    ASSERT_EQ(std::tuple{1, 2, 3, 100}, m.value().to_tuple());
    m.push(2, 4, 6, 200);
    m.push(3, 6, 9, 300);
    ASSERT_EQ(std::tuple{2, 4, 6, 200}, m.value().to_tuple());
    m.push(4, 8, 12, 400);
    m.push(5, 10, 15, 500);
    ASSERT_EQ(std::tuple{3, 6, 9, 300}, m.value().to_tuple());
    m.push(6, 12, 18, 600);
    m.push(7, 14, 21, 700);
    ASSERT_EQ(std::tuple{4, 8, 12, 400}, m.value().to_tuple());
    m.push(8, 16, 24, 800);
    ASSERT_EQ(std::tuple{5, 10, 15, 500}, m.value().to_tuple());
};
