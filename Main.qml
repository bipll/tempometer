import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.15
import QtQuick.Shapes 1.15
import QtQuick.Window 2.15
import QtLocation 6.6
import 'Format.js' as Fmt
import 'Util.js' as U
import 'MapLayer.js' as ML
import QtPositioning 5.15

GridLayout {
    columnSpacing: 2
    rowSpacing: 1

    id: main

    property string pluginName: 'osm'
    property list<PluginParameter> pluginParams: [
        PluginParameter { name: "osm.useragent"; value: "tempometer" }
    ]

    property Plugin plugin: Plugin {
        name: main.pluginName
        parameters: main.pluginParams
    }

    property color speedColor: 'black'
    property bool scale1: true

    property var remnants: []

    Connections {
        target: expected
        function onWhereChanged() {
            syncMap(expected.coord, expected.spd)
            let touched = 0
            while(route.length > 0 && total.expectedCoord.distanceTo(route[0].coordinate) <= cfg.wptSensitivity) {
                ++touched
                --route.shift().planned
                if(total.recording && cfg.startLapsOnWpts) newLap()
            }
            if(touched) {
                poiLayer.remove(...arrows.splice(0, touched))
                if(arrows.length > 0) arrows[0].base = Qt.binding(() => map.center)
                arrows.forEach((arrow, number) => { arrow.number = number })
            }
        }
    }
    Connections {
        target: total
        function onCheckpointChanged() {
            addMilestone(total.checkpoint, total.milestoneAchieved, total.checkpointDirection, current.trackNo)
        }
        function onRecordingChanged() {
            arrowhead.path = [total.expectedCoord]
            if(total.recording && !isNaN(total.bearing)) recon.start()
            else recon.stop()
        }
        function onCaptured() {
            save.saved()
        }
        function onBearingChanged() {
            if(!isNaN(total.bearing)) character.bearing = total.bearing
        }
    }

    Connections {
        target: current
        function onTrackNoChanged(trackNo) {
            iterNumerus = Fmt.roman(trackNo)
            console.log('TrackNo:', iterNumerus)
        }
    }

    Connections {
        target: replay
        function onNextPoint(when, where) { remnants.push([when, where]) }
    }

    property var laps
    property var lapLogs: []
    property string logAbove: ''

    property var currentCurve
    property var curves
    property string iterNumerus: ''

    function showLaps(curr) {
        return recording? recentLapLog + Fmt.asLap(curr) : recentLapLog
        /*
        if(recentLapLog === null) return recording? Fmt.asLap(curr) : ''
        return recording? recentLapLog + Fmt.asLap(curr) : recentLapLog
        */
    }

    property string trackLog: ''

    function archive(lap) {
        laps.push({
            timestamp: lap.timestamp,
            trackNo: lap.trackNo,
            dist: lap.dist,
            time: lap.time,
            avg: lap.avg,
            mtime: lap.mtime,
            mavg: lap.mavg
        })
        if(lapLogs.length > 3) lapLogs.shift()
        lapLogs.push(Fmt.asLap(lap))
        logAbove = lapLogs.reduce((s, l) => s + l + '\n', '')
        console.log('Above:', logAbove)
    }

    function paintAt(layer, object, visible = true) {
        map.removeMapItemGroup(layer)
        layer.data.push(object)
        if(visible) map.addMapItemGroup(layer)
    }

    function unpaintAt(layer, object, visible = true) {
        let dta = layer.data
        let changed = false
        for(let idx = 0; idx < dta.length; ++idx) if(dta[idx] === object) {
            console.log("found at idx", idx)
            let omg = dta
            dta.splice(idx, 1)
            changed = true
            break
        }
        if(changed) {
            if(visible) map.removeMapItemGroup(layer)
            layer.data = dta
            if(visible) map.addMapItemGroup(layer)
        }
    }

    property Map mapWidget: map
    Item {
        id: mapDisplay
        Layout.row: 0
        Layout.column: 0
        Layout.columnSpan: 4
        Layout.preferredHeight: 557 - tallLed - shortLed
        Layout.fillWidth: true
        Layout.fillHeight: true

        MapView {
            Map {
                id: map
                plugin: main.plugin
                height: parent.height
                width: parent.width - editRoute.width
                anchors.left: parent.left
                
                zoomLevel: maximumZoomLevel
                
                property int acceptedGestures: MapGestureArea.PanGesture | MapGestureArea.FlickGesture | MapGestureArea.PinchGesture //| MapGestureArea.RotationGesture | MapGestureArea.TiltGesture
                /* gesture.acceptedGestures: acceptedGestures
                gesture.flickDeceleration: 3000
                gesture.enabled: true
                gesture.onPanStarted: tethered = false
                gesture.onFlickStarted: tethered = false
            
                    gesture.onPinchStarted: tethered = false */
                
                Text {
                    id: fix
                    anchors.left: parent.left
                    anchors.top: parent.top
                    text: total.fix? 'FIX' : 'NOFIX'
                    color: total.fix? total.extrapolated? '#A0A000' : '#00A000' : '#FF0000'
                }
                Text {
                    id: coordinates
                    anchors.right: parent.right
                    anchors.top: parent.top
                    font.family: 'Droid Sans Mono'
                    text: Fmt.asCoordinates(total.expectedCoord.latitude, total.expectedCoord.longitude)
                    style: Text.Outline
                    styleColor: U.outlineColor
                }
                Text {
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 28
                    x: 170
                    font.family: 'Droid Sans Mono'
                    style: Text.Outline
                    styleColor: U.outlineColor
                    text: trackLog
                }
                
                Text {
                    id: distanceFromHere
                    anchors.right: parent.right
                    anchors.top: coordinates.bottom
                    visible: !tethered
                    font.family: 'Droid SansMono'
                    style: Text.Outline
                    styleColor: U.outlineColor
                    text: Fmt.dist(map.center.distanceTo(total.expectedCoord))
                }
                
                Text {
                    anchors.left: parent.left
                    anchors.top: fix.bottom
                    visible: true //wpts.length > 0
                    font.family: 'Droid SansMono'
                    style: Text.Outline
                    styleColor: U.outlineColor
                    text: itinerary(route, map.center)
                }
                
                Text {
                    visible: !tethered
                    opacity: 0.3
                    anchors.horizontalCenter: map.horizontalCenter
                    anchors.top: map.top
                    text: Fmt.asLon(map.center.longitude)
                    font.family: 'Droid Sans Mono'
                    style: Text.Outline
                    styleColor: U.outlineColor
                }
                Text {
                    visible: !tethered
                    opacity: 0.3
                    anchors.right: map.right
                    anchors.verticalCenter: map.verticalCenter
                    text: Fmt.asLat(map.center.latitude)
                    font.family: 'Droid Sans Mono'
                    style: Text.Outline
                    styleColor: U.outlineColor
                }
                Shape {
                    visible: !tethered
                    anchors.centerIn: parent
                    ShapePath {
                        strokeColor: '#4D00FF00'
                        fillColor: 'transparent'
                        startX: -5; startY: 0
                        PathLine { x: 5; y: 0 }
                        PathMove { x: 0; y: -5 }
                        PathLine { x: 0; y: 5 }
                    }
                }
                
                Button {
                    anchors.left: parent.left
                    anchors.bottom: px128.top
                    anchors.bottomMargin: 30
                    text: 'Recentre'
                    enabled: !tethered
                    visible: !tethered
                    onClicked: tethered = true
                    opacity: 0.55
                    height: width * 1.2
                }
                
                Ruler {
                    id: px128
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.leftMargin: 16
                    anchors.bottomMargin: 32
                    scaleLevel: 40075016.686 * Math.cos(map.center.latitude / 180 * Math.PI) / (2 ** (map.zoomLevel + 1))
                }
                property int zoomLayer
                
                MapPolyline {
                    visible: recording
                    id: arrowhead
                    line.color: U.defaultColor
                    line.width: 7
                    path: [map.center]
                }
                
                property var tappedPoint
                
                Menu {
                    id: contextMenu
                    onAboutToShow: map.gesture.acceptedGestures = 0
                    onClosed: map.gesture.acceptedGestures = map.acceptedGestures
                    MenuItem {
                        text: Fmt.asCoordinates(map.tappedPoint.latitude, map.tappedPoint.longitude, ' ')
                        onTriggered: contextMenu.close()
                    }
                    
                    MenuItem {
                        text: "Mark wpt"
                        font.pointSize: 21
                        onTriggered: addWaypoint(map.tappedPoint)
                    }
                    
                    MenuItem {
                        text: "Save wpt"
                        font.pointSize: 21
                        onTriggered: map.savePoi()
                    }
                    
                    MenuItem { text: 'Edit rt'; font.pointSize: 21; onTriggered: editRt() }
                }
                
                Dialog {
                    id: poiData
                    
                    anchors.centerIn: parent
                    width: 320
                    height: 300
                    property bool addToMap: true
                    function show(add = true) { addToMap = add; open() }
                    property var existingPoi: null
                    
                    property var where: existingPoi === null? map.tappedPoint : existingPoi.coordinate
                    
                    header: Text { horizontalAlignment: Text.AlignHCenter; text: Fmt.asCoordinates(poiData.where.latitude, poiData.where.longitude, ' ') }
                    
                    ColumnLayout {
                        spacing: 5
                        TextArea {
                            id: wptName
                            Layout.preferredWidth: 300
                            Layout.preferredHeight: 40
                            font.pointSize: 21
                            text: poiData.existingPoi === null? '' : poiData.existingPoi.name
                            placeholderText: 'name'
                            background: Rectangle {
                                color: 'transparent'
                                border.color: 'darkgrey'
                                border.width: 3
                            }
                        }
                        ScrollView {
                            Layout.preferredWidth: 300
                            Layout.preferredHeight: 160
                            TextArea {
                                id: wptDescription
                                width: 300
                                height: 160
                                font.pointSize: 21
                                placeholderText: 'description'
                                wrapMode: TextArea.Wrap
                                text: poiData.existingPoi === null? '' : poiData.existingPoi.description
                                background: Rectangle {
                                    color: 'transparent'
                                    border.color: 'darkgrey'
                                    border.width: 3
                                }
                            }
                        }
                    }
                    standardButtons: Dialog.Ok | Dialog.Cancel
                    onAccepted: {
                        let newPoi = existingPoi === null
                        console.log('Saving poi: fresh =', newPoi, wptName.text, wptDescription.text)
                        let rv = addPoi(where, wptName.text, wptDescription.text, newPoi)
                        if(!newPoi) {
                            console.log('new id:', rv)
                            existingPoi.dbId = rv
                            existingPoi.name = wptName.text
                            existingPoi.description = wptDescription.text
                            existingPoi = null
                        }
                        wptName.clear()
                        wptDescription.clear()
                    }
                }
                
                TapHandler {
                    id: longtap
                    property bool resetTethered: false
                    onPressedChanged: resetTethered = pressed && tethered
                    
                    onLongPressed: {
                        if(resetTethered) {
                            map.gesture.enabled = false
                            tethered = true
                            map.gesture.enabled = true
                        }
                        map.tappedPoint = map.toCoordinate(point.position)
                        contextMenu.popup()
                    }
                    
                    onCanceled: point.accepted = false
                    onDoubleTapped: point.accepted = false
                    onGrabChanged: point.accepted = false
                    onSingleTapped: point.accepted = false
                    onTapped: point.accepted = false
                    
                }
                
                onZoomLevelChanged: {
                    px128.scaleLevel = 40075016.686 * Math.cos(center.latitude / 180 * Math.PI) / (2 ** (map.zoomLevel + 1))
                    if(!tethered) {
                        var ezl = character.expectedZoomLevel
                        if(ezl < zoomLevel - 1.3) character.zoomLevel = zoomLevel - 1.3
                        else if(ezl > zoomLevel + 1.3) character.zoomLevel = zoomLevel + 1.3
                        else character.zoomLevel = ezl
                    }
                    findZoomLayer()
                    //console.log('Zoom:', zoomLevel, zoomLayer)
                }
                onCenterChanged: px128.scaleLevel = 40075016.686 * Math.cos(center.latitude / 180 * Math.PI) / (2 ** (map.zoomLevel + 1))
                
                function addLayeredItem(layer, item) {
                    itemLayers[layer].add(item)
                }
                
                property var itemLayers: []
                
                function findZoomLayer() {
                    let newLayer = Math.min(Math.max(Math.ceil(14 - zoomLevel), 0), U.mapLayers)
                    if(zoomLayer === newLayer) return
                    for(let itemLayer of itemLayers) itemLayer.toggle(newLayer)
                    zoomLayer = newLayer
                }
                
                function focusAt(layer) {
                    tethered = false
                    fitViewportToMapItems([...layer.items])
                }
                
                function savePoi(existing) {
                    if(existing !== undefined) console.log('Saving existing poi', existing.name, existing.description)
                    if(existing !== undefined) poiData.existingPoi = existing
                    poiData.open()
                }
                
                Component.onCompleted: { for(let i = 0; i < U.mapLayers; ++i) itemLayers.push(new ML.MapLayer(map, i)) }
                
                function sweepItems(items) {
                    console.log('Sweeping layer, nItems =', items.length)
                    for(let item of items) { console.log('removing item', item); removeMapItem(item) }
                }
                
                function sweepZoomLayers() {
                    for(let i = 0; i < U.mapLayers; ++i) itemLayers[i].clear()
                }
                
                function show(item) { console.log('Map: adding', item); addMapItem(item) }
                function hide(item) { console.log('Map: removing', item); removeMapItem(item) }
            }
        }
            
        EditRt {
            id: editRoute
            height: parent.height
            anchors.right: parent.right
        }
    }

    Component.onCompleted: {
        map.addMapItem(character)
        poiLayer.add(...db.pois().map(
                         (poi) => poiFactory.createObject(map, {
                                                              dbId: poi.rowid,
                                                              coordinate: QtPositioning.coordinate(poi.lat, poi.lon),
                                                              name: poi.name,
                                                              description: poi.description,
                                                              planned: false
                                                          })
                         )
                     )
    }

    property var lapsLayer: new ML.MapLayer(map)
    property var reviewLayer: new ML.MapLayer(map)
    property var poiLayer: new ML.MapLayer(map)
    property var trackLayer: new ML.MapLayer(map)
    property var naviLayer: new ML.MapLayer(map)

    property Component checkpointFactory: Component { Checkpoint {} }
    property Component quiver: Component { Arrow { device: map } }
    property Component wptFactory: Component { Waypoint {} }
    property Component poiFactory: Component { Poi {} }

    function milestoneLayer(dist) {
        let layers = [250, 500, 1000, 2500, 5000, 10000, 25000, 50000, 100000, 250000]
        const small = layers[0] / 2
        let i = layers.length
        while(i > 0) {
            let precision = dist % layers[--i]
            if(-small < precision && precision < small) break
        }
        return i
    }

    function addMilestone(coord, dist, angle, trackNo) {
        let layer = milestoneLayer(dist)
        console.log('====================================================================================================== Milestone. dist:', dist, 'layer:', layer)
        var milestone = checkpointFactory.createObject(map,
                                                       {
                                                           coordinate: coord,
                                                           text: (dist / cfg.mile).toString(),
                                                           otherText: iterNumerus,
                                                           z: trackNo,
                                                           direction: angle,
                                                           mapBearing: map.bearing,
                                                           color: U.lineColor(trackNo)
                                                       })

        map.addLayeredItem(layer, milestone)
    }

    property int tallLed: 119
    property int shortLed: 59
    MapQuickItem {
        id: character
        coordinate: total.expectedCoord
        zoomLevel: map.maximumZoomLevel
        property real expectedZoomLevel: 0.0
        anchorPoint.x: 0 // avatar.width / 2
        anchorPoint.y: 0 // avatar.height / 2
        property real bearing: 0
        sourceItem: Item {
            id: avatar
            Text {
                x: -width * 0.5
                y: -height * 0.5
                text: '@'
                z: 1048576
                rotation: character.bearing
                Behavior on rotation { RotationAnimation { duration: 250; direction: RotationAnimation.Shortest }}
            }
        }
    }

    Timer {
        id: recon
        interval: 100
        repeat: true
        onTriggered: {
            let recentPoint = arrowhead.coordinateAt(arrowhead.pathLength() - 1)
            let nextPoint = recentPoint.atDistanceAndAzimuth(total.spd * 0.1, total.bearing)
            arrowhead.addCoordinate(nextPoint)
            syncMap(nextPoint, false)
        }
    }

    property bool tethered: true
    property bool recording: total.recording
    onTetheredChanged: {
        syncMap(total.expectedCoord)
        if(!tethered) character.expectedZoomLevel = expectedZoomLevel()
    }

    function expectedZoomLevel(spd) {
        return isNaN(spd) || spd < 4?
                    map.maximumZoomLevel :
                    map.maximumZoomLevel - (Math.log2(spd) - 2) * 1.5 /// U.l23
    }

    function syncMap(coord, spd, actual = true) {
        if(tethered) {
            map.zoomLevel = expectedZoomLevel(spd)
            character.zoomLevel = map.zoomLevel
            if(!total.recording || isNaN(total.where.bearing) || isNaN(total.spd) || total.spd < tooSlow) map.center = where
            else {
                let dist = total.spd * total.spd / 4 // * (1 - 2 ** (- total.spd * total.spd / 625))
                map.center = where.atDistanceAndAzimuth(dist, total.bearing)
            }
        }
        character.coordinate = QtPositioning.coordinate(where.latitude, where.longitude, where.altitude)
        if(actual && total.recording && !isNaN(total.bearing)) {
            arrowhead.path = [total.expectedCoord]
            recon.start()
        }
    }

    property color borders: '#101010'

    Border { Layout.row: 1; Layout.fillWidth: true }
    Border { Layout.row: 1; Layout.column: 3; Layout.fillWidth: true }

    // Row 1
    Led {
        Layout.row: 2
        Layout.preferredWidth: 190
        Layout.preferredHeight: tallLed
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "SPD"
        value: Value { value: total.spd; time: total.time }
        textColor: speedColor
    }

    property color greenButton: '#E0FFE0'
    property color redButton: '#FFE0E5'
    property color yellowButton: '#FFFFE0'
    property color greyButton

    Button {
        id: lapStopSave
        Layout.preferredWidth: 96
        Layout.columnSpan: 2
        Layout.fillWidth: true
        Layout.fillHeight: true
        text: 'LIB'
        font.pointSize: 24

        function start() { text = 'LAP'; background.color = greenButton }
        function pause() { text = 'STOP'; background.color = redButton }
        function stop() { text = 'SAVE'; background.color = greyButton }
        function reset() { text = 'LIB'; background.color = greyButton }

        onClicked: lapStopSaveClicked()

        Component.onCompleted: greyButton = background.color
    }
    Led {
        Layout.preferredWidth: 190
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "TIM"
        format: Fmt.asTime
        fontScale: 0.36
        value: Value { value: total.time; time: total.time }
    }

    Border { Layout.row: 3; Layout.fillWidth: true }
    Border { Layout.row: 3; Layout.column: 3; Layout.fillWidth: true }

    // Row 2
    Led {
        Layout.row: 4
        Layout.preferredHeight: tallLed
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "AVG"
        value: Value { value: total.avg; time: total.time }
    }
    Button {
        id: startPauseRec
        Layout.columnSpan: 2
        Layout.fillWidth: true
        Layout.fillHeight: true
        font.pointSize: 24
        function start() { text = 'PAUSE'; background.color = yellowButton }
        function pause() { text = 'REC'; background.color = greenButton }
        function stop() { text = 'START'; background.color = greenButton }
        onClicked: startPauseRecClicked()

        Component.onCompleted: stop()
    }

    Led {
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "DST"
        format: Fmt.asDist
        fontScale: value < 1000 || value >= 10000000? 0.36 : 0.4
        value: Value { value: total.dist; expectedValue: total.expectedDist; time: total.time }
    }

    Border { Layout.row: 5; Layout.fillWidth: true }
    Border { Layout.row: 5; Layout.column: 3; Layout.fillWidth: true }

    // Row 3
    Led {
        Layout.row: 6
        Layout.columnSpan: 2
        Layout.preferredHeight: shortLed
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "MAVG"
        value: Value { value: total.mavg; time: total.mtime }
    }
    Led {
        id: altimeter
        Layout.columnSpan: 2
        Layout.fillWidth: true
        Layout.fillHeight: true
        label: "ALT"
        format: Fmt.asAlt
        value: Value { value: total.expectedCoord.altitude; time: timeless }
    }

    property var tracks: [current, total]
    property var buttons: [lapStopSave, startPauseRec]
    property var devices: [...tracks, ...buttons]
    function started() {
        console.log('Tempometer from QML:', "Main::started")
        console.log('lapsLayer.clear')
        lapsLayer.clear()
        // reviewLayer.clear()
        trackLayer.clear()
        map.sweepZoomLayers()
        //draw(character)
        tethered = true
        curves = []
        laps = []
        altimeter.value.max = altimeter.value.value

        for(let dev of devices) dev.start()
        currentCurve = curveFactory.createObject(null)
        lapsLayer.add(currentCurve)
        total.timestamp = current.timestamp
        lapLogs = []
        logAbove = ''
        trackLog = Qt.binding(() => logAbove + Fmt.asLap(current))
    }
    function paused() {
        currentCurve.archive()
        for(let dev of devices) dev.pause()
    }

    property bool oldTethered
    property var oldViewport
    function stopped() {
        for(let button of buttons) button.stop()
        trackLog = trackLog
        archive(current)
        curves.push(currentCurve)

        let topleftmost = map.toCoordinate(Qt.point(0, 0))
        let bottomrightmost = map.toCoordinate(Qt.point(map.width, map.height))
        let leftmost = topleftmost.longitude
        let rightmost = bottomrightmost.longitude
        if(rightmost < leftmost) rightmost += 360
        let topmost = topleftmost.latitude
        let bottommost = bottomrightmost.latitude
        let trackWidth = total.east - total.west
        let trackHeight = total.north - total.south
        let expectedLeft = total.west - trackWidth * 0.1
        let expectedRight = total.east + trackHeight * 0.1
        if(expectedRight < expectedLeft) expectedRight +=360
        // TODO: coordinates near the -180|+180 meridian should be handled more accurately ... next time
        // luckily poles are free of this issue
        let expectedTop = total.north + trackHeight * 0.1
        let expectedBottom = total.south - trackHeight * 0.1
        if(expectedLeft < leftmost || expectedRight > rightmost || expectedTop > topmost || expectedBottom < bottommost) {
            oldTethered = tethered
            oldViewport = map.visibleRegion
            lapsLayer.focus()
            map.findZoomLayer()
        }

        save.laps = laps
        save.track = total
    }

    function newLap() {
        curves.push(currentCurve.archive())
        //draw(currentCurve)

        archive(current)
        total.unpause()
        current.nextLap()

        currentCurve = curveFactory.createObject(null)
        lapsLayer.add(currentCurve)

        for(let button of buttons) button.start()
        let prevTracks = laps.slice(-4).reduce((s, l) => s + Fmt.asLap(l) + '\n')
    }

    signal lapStopSaveClicked
    signal startPauseRecClicked

    function draw(item) { map.addMapItem(item) }
    function erase(item) { map.removeMapItem(item) }
    function clear() { map.clearMapItems() }

    function reset() { lapStopSave.reset() }
    function unsaved() { lapStopSave.stop() }

    property var logText: ['', '']
    function log(...args) {
        logText[0] = logText[1]
        logText[1] = args.join(' ')
        logConsole.text = logText[0] + '\n' + logText[1]
    }

    /*
    MapItemGroup {
        id: review

        MapPolyline {
            id: currentEcho
            line.width: 8
        }
        MapPolygon {
            id: speedGraph
            border.color: '#80C0FFC0'
            color: '#40D0FFD0'
        }
        MapPolyline {
            id: averageSpeed
            line.color: '#80C0FFC0'
        }
        MapPolygon {
            id: heightGraph
            border.color: '#80C0C0FF'
            color: '#40D0D0FF'
        }
        MapPolyline {
            id: maxSpeedPost
            line.color: 'red'
        }
        MapPolyline {
            id: maxHeightPost
            line.color: 'red'
        }
    }
    */

    property var currentEcho: null
    property var speedGraph: null
    property var averageSpeed: null
    property var heightGraph: null

    function midray(one, two) {
        let diff = two - one
        if(diff > 180) two -= 360
        else if(diff <= -180) two += 360
        return (one + two) / 2
    }

    function showReview(trackId, laps) {
        reviewLayer.clear()
        recallTrack(trackId, laps)
        console.log('remnants:', remnants.length)
        if(remnants.length === 0) return

        currentEcho = curveFactory.createObject(map)
        highlight.target = currentEcho
        highlight.running = true

        speedGraph = polygonFactory.createObject(map)
        speedGraph.border.color = '#80C0C0FF'
        speedGraph.color = '#40D0FFD0'

        averageSpeed = curveFactory.createObject(map)
        averageSpeed.line.color = '#80FFFFC0'

        //heightGraph = polygonFactory.createObject(map)
        //heightGraph.border.color = '#80C0C0FF'
        //heightGraph.color = '#40D0D0FF'

        if(remnants.length < 3) {
            for(let rem of remnants) currentEcho.addCoordinate(rem)
            reviewLayer.addItem(currentEcho)
            reviewLayer.focus()
            return
        }

        let minHeight = NaN
        let minHeightAt = undefined

        let maxHeight = NaN
        let maxHeightAt = undefined

        let maxSpeed = 0
        let maxSpeedAt = 0

        let point = remnants[0][1]
        let nextPoint = remnants[1][1]
        currentEcho.addCoordinate(point)
        speedGraph.addCoordinate(point)
        //heightGraph.addCoordinate(point)
        let dist = 0
        let nextStride = point.distanceTo(nextPoint)
        let time = 0
        let prevDt = remnants[1][0] - remnants[0][0]

        let postBearing = point.azimuthTo(nextPoint)
        console.log('Bearing:', postBearing)
        let bearings = [postBearing]
        console.log("postBearing:", postBearing)
        for(let i = 1; i < remnants.length - 1; ++i) {
            let prevPoint = point
            point = nextPoint
            nextPoint = remnants[i + 1][1]

            let omg = prevDt
            let dt = remnants[i + 1][0] - remnants[i][0]
            time += prevDt

            let preBearing = postBearing
            postBearing = point.azimuthTo(nextPoint)
            let bearing = midray(preBearing, postBearing)
            //console.log('Bearing:', bearing, ' (', preBearing, '->', postBearing, ')')
            bearings.push(bearing)

            let stride = nextStride
            nextStride = point.distanceTo(nextPoint)

            if(i > 154 && i < 160 || i > 234 && i < 240) {
                console.log('Distance:', point.distanceTo(nextPoint), prevPoint.distanceTo(nextPoint), dt, (stride + nextStride) * 1000 / (omg + dt), i)
            }

            let speed = (stride + nextStride) * 1000 / (prevDt + dt)
            prevDt = dt

            //if(maxSpeed < speed) {
                //maxSpeed = speed
                //maxSpeedAt = i
                //console.log('Max speed at', i, 'at', speed)
                //console.log('\tdist =', dist)
                //console.log('\tstride =', stride, 'prevDt =', prevDt)
                //console.log('\tnextStride =', nextStride, 'dt = ', dt)
            //}
            dist += stride

            currentEcho.addCoordinate(point)

            let yawSin = Math.sin(postBearing - preBearing) * 5.5
            if(Math.abs(speed * 5 * yawSin) < nextStride) {
                speedGraph.addCoordinate(point.atDistanceAndAzimuth(speed * 5, bearing - 90))
            }
            //if(maxSpeed < speed) {
                //maxSpeed = speed
                //maxSpeedAt = i
            //}

            if(time >= 5000) {
                let avg = dist * 5000 / time
                if(Math.abs(avg * yawSin) < nextStride) {
                    averageSpeed.addCoordinate(point.atDistanceAndAzimuth(avg, bearing - 90))
                }
            }
            //heightGraph.addCoordinate(point)
            //let height = point.altitude
            //if(!(maxHeight >= height)) { maxHeight = height; maxHeightAt = i }
            //if(!(minHeight <= height)) { minHeight = height; minHeightAt = i }
        }

        point = nextPoint
        time += prevDt
        dist += nextStride

        currentEcho.addCoordinate(point)
        speedGraph.addCoordinate(point)
        if(time >= 5000) {
            let avg = dist * 1000 / time
            console.log('Avg:', dist, time, avg, averageSpeed)
            //averageSpeed.addCoordinate(point.atDistanceAndAzimuth(avg * 5, bearing - 90))
        }
        //heightGraph.addCoordinate(point)

        let prevPoint = remnants[remnants.length - 1][1]
        for(let i = remnants.length - 1; i > 0;) {
            point = remnants[--i][1]
            speedGraph.addCoordinate(point)
            //if(!isNaN(maxHeight)) {
                //let height = point.altitude
                //heightGraph.addCoordinate(point.atDistanceAndAzimuth(height - minHeight), bearings[i])
            //}
        }

        reviewLayer.add(speedGraph, averageSpeed, currentEcho)
        reviewLayer.focus()
        //map.fitViewportToMapItems([speedGraph, averageSpeed, currentEcho])
        //console.log('Zoom:', map.zoomLevel, map.zoomLayer)
    }

    function hideReview() {
        if(speedGraph) {
            map.removeMapItem(speedGraph)
            speedGraph = null
        }
        if(averageSpeed) {
            map.removeMapItem(averageSpeed)
            averageSpeed = null
        }
        if(heightGraph) {
            map.removeMapItem(heightGraph)
            heightGraph = null
        }
        if(currentEcho) {
            map.removeMapItem(currentEcho)
            currentEcho = null
        }
        highlight.running = false
        highlight.target = null
        remnants = []
    }

    function recallTrack(trackId, laps, lap) {
        replay.tape = trackId
        if(typeof lap === 'number' || lap instanceof Number) replay.replayLap(lap, true)
        else replay.fastForward(laps)
    }

    property color reviewColor

    Flickering {
        id: highlight
        running: false
    }

    property var route: []
    property var thousandLi: routeLength(route)
    property var arrows: []

    onRouteChanged: { if(editRoute.visible) editRt() }

    function routeLength() {
        let point = total.expectedCoord
        let rv = 0
        for(let nextPoint of route) {
            rv += point.distanceTo(nextPoint.coordinate)
            point = nextPoint.coordinate
        }
        return rv
    }

    function addWaypoint(coord, key = '') {
        let addToRoute = cfg.addNewWpts
        let poi = poiFactory.createObject(map, {coordinate: coord, dbId: key})
        poiLayer.add(poi)
        if(addToRoute) enroute(poi)
        return poi
    }

    function addPoi(coord, name, description, putOnMap = true) {
        let dbId = db.addPoi(coord.latitude, coord.longitude, name, description)
        return putOnMap? addWaypoint(coord, dbId) : dbId
    }

    function itinerary() {
        if(route.length === 0) return ''
        let start = map.center
        let rv = Fmt.dist(thousandLi) + ' TO GO\n'
        for(let i = 0; i < 4 && i < route.length; ++i) {
            if(i > 0) rv += '\n+'
            let goal = route[i].coordinate
            rv += Fmt.dist(start.distanceTo(goal))
            start = goal
        }
        return rv
    }

    function enroute(poi) {
        console.log('Enroute: routelen', route.length, 'arrows len', arrows.length)
        route.push(poi)
        let routeLen = route.length
        let arrow = routeLen === 1?
                quiver.createObject(map, {number: 1, base: Qt.binding(() => map.center), goal: poi.coordinate}) :
                quiver.createObject(map, {number: routeLen, base: route[routeLen - 2].coordinate, goal: poi.coordinate})
        console.log('arrow number', arrow.number)
        arrows.push(arrow)
        console.log('Now', arrows.length, 'arrows')
        poiLayer.add(arrow)
        ++poi.planned
        route = route
    }

    function deroute(poi) {
        for(var idx = 0; idx < route.length; ++idx) if(route[idx] === poi) derouteAt(idx)
    }

    function derouteAt(idx) {
        --route[idx].planned
        console.log('Planned:', route[idx].planned)
        route.splice(idx, 1)
        if(idx < route.length) {
            if(idx) arrows[idx + 1].base = arrows[idx].base
            else arrows[1].base = Qt.binding(() => map.center)
        }
        poiLayer.remove(arrows[idx])
        arrows.splice(idx, 1)
        if(idx > 0 && idx < route.length && route[idx - 1] === route[idx]) {
            route.splice(idx, 1)
            if(idx < route.length) arrows[idx + 1].base = arrows[idx].base
            poiLayer.remove(arrows[idx])
            arrows.splice(idx, 1)
        }
        while(idx < route.length) arrows[idx++].number = idx
        route = route
    }

    function editRt() {
        let refocus = !editRoute.visible
        editRoute.population = route.length
        editRoute.popup()
        if(arrows.length > 0) {
            for(let arrow of arrows) arrow.length = NaN
            arrows[0].base = total.expectedCoord
        }
        tethered = false
        if(!refocus) {
            let needsFocus = false
            for(let routept of route) if(!map.visibleRegion.contains(routept.coordinate)) {
                refocus = true
                break
            }
        }

        if(refocus) map.fitViewportToMapItems([...route, character])
    }

    function buildTrack() {
        console.log('TODO')
    }

    function swapPois(one, two) {
        if(one === two) return  // when one equals two everything is possible and nothing makes sense
        if(one > two) {
            let least = two
            two = one
            one = least
        }

        let first = route[one]
        let second = route[two]
        let arrow1 = arrows[one]
        let arrow2 = arrows[two]
        route[one] = second
        route[two] = first
        if(one + 1 === two) arrow1.goal = arrow2.base = second.coordinate
        else arrow1.goal = arrows[one + 1].base = second.coordinate

        arrow2.goal = first.coordinate

        if(two < route.length - 1) arrows[two + 1].base = first.coordinate
        route = route
    }
}
