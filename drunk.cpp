#include "drunk.h"
#include <cstdlib>
#include <QDateTime>

Drunk::Drunk(QObject *parent)
    : QObject{parent}
{
}

void Drunk::startUpdates() {
    connect(&roaming, &QTimer::timeout, this, [&]{
        emit positionUpdated({where, QDateTime::currentDateTime()});
        where = where.atDistanceAndAzimuth(stride, bearing);
        bearing += std::rand() / 100000000 - 10;
        if(--count == 0) {
            count = 5;
            stride += 1;
        }
    });
    roaming.start(1000);
}
