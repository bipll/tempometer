#ifndef DRUNK_H
#define DRUNK_H

#include <QObject>
#include <QGeoPositionInfo>
#include <QTimer>

class Drunk : public QObject
{
    Q_OBJECT
public:
    explicit Drunk(QObject *parent = nullptr);

public slots:
    void startUpdates();

signals:
    void positionUpdated(QGeoPositionInfo const &);
    void updateTimeout();

private:
    QGeoCoordinate where{52, 0};
    double bearing = 75;
    double stride = 12;
    std::size_t count{5};

    QTimer roaming;
};

#endif // DRUNK_H
