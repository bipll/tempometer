import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

Rectangle {
    id: block
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.margins: 10
    color: 'transparent'
    border.color: 'lightsteelblue'

    required property string section
    Label {
        id: label
        x: 0
        y: 0
        text: section
        font.pointSize: 12
        padding: 3
        background: Rectangle {
            anchors.fill: parent
            color: 'lightsteelblue'
            radius: 3
        }
    }
    //Component.onCompleted: label.background.color = 'lightgrey'
}
