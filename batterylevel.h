#ifndef BATTERYLEVEL_H
#define BATTERYLEVEL_H

#include <QObject>
#include <QtGlobal>
#include <qqml.h>

class BatteryLevel: public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint level READ level WRITE setLevel NOTIFY levelChanged)
    QML_ELEMENT

public:
    BatteryLevel(QObject *parent = nullptr);

    uint level();
    void setLevel(uint);

public slots:
    void update();

signals:
    void levelChanged(uint newLevel);

private:
    uint read() const;

    uint currLevel;
};

#endif // BATTERYLEVEL_H
