#include <cstring>
#include <startup.h>

#include <QDebug>
#include <QFile>
#include <QStandardPaths>

int main(int argc, char *argv[]) {
    return argc > 1? runSvc(argc, argv) : runApp(argv);
}
