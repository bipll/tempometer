import QtQuick 2.0

QtObject {
    property real value: NaN
    property real expectedValue: NaN
    property real max: NaN
    property real expectedMax: NaN
    property bool active: true
    required property real time
    property real timeless: avgDelay
    property bool locked: false

    function restart() {
        max = NaN
        expectedMax = NaN
    }

    onValueChanged: {
        locked = true
        expectedValue = value
        locked = false
        if(active) {
            if(time >= timeless) {
                if(isNaN(max) || max < value) {
                    max = value
                    expectedMax = value
                }
            }
            else max = NaN
        }
    }
    onExpectedValueChanged: {
        if(active && !locked) {
            if(time >= timeless) {
                if(isNaN(expectedMax) || expectedMax < expectedValue) expectedMax = expectedValue
            }
        }
    }
}
