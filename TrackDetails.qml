import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt.labs.qmlmodels 1.0

Item {
    id: trackDetails

    width: 200
    height: 100

    Rectangle {
        anchors.fill: parent
        color: '#F8F8F8'
    }

    TableView {
        id: trackData
        z: 1
        model: TableModel {
            TableModelColumn { display: 'omg' }
            rows: [{omg: 'nyan'}, {omg: 'wat'}]
        }
        delegate: Text { text: display }
    }

    property var trackSummary
    property var laps

    function ctor() {}
}
