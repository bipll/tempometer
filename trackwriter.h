#ifndef TRACKWRITER_H
#define TRACKWRITER_H

#include <cmath>
#include <limits>
#include <type_traits>
#include <vector>

#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QTimer>
#include <QQmlPropertyMap>
#include <QQuickItem>
#include <QQuickItemGrabResult>
#include <QImage>

#include <rep_tracksegment_replica.h>

#include "defs.h"

class TrackWriter : public QObject
{
    Q_OBJECT

    using Super = QObject;

public:
    explicit TrackWriter(QCoreApplication *parent);
    void listen(TrackSegmentReplica *);

public slots:
    void keep(QString const &dirname, quint32 from, quint32 to);

private:
    void openStdOut();
    template<class T> bool write(T t) { return stdOut.write(reinterpret_cast<char const *>(&t), sizeof(T)); }

    bool enabled{true};
    unsigned int lapNo;
    QFile stdOut;
    bool rec;
    TrackSegmentReplica *outport;
    QTimer transpose;


    QTimer heartbeat;
    //QMetaObject::Connection heartbeatTimeout;
};

#endif // TRACKSEGMENT_H
