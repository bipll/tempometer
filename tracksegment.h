#ifndef TRACKSEGMENT_H_10b27f976ddeb25d24ccb81fd5e7d6eaf0e7c88c
#define TRACKSEGMENT_H_10b27f976ddeb25d24ccb81fd5e7d6eaf0e7c88c

#include <cmath>
#include <limits>
#include <type_traits>
#include <vector>
#include <deque>

#include <QCoreApplication>
#include <QFile>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QTimer>
#include <QQmlPropertyMap>
#include <QQuickItem>
#include <QQuickItemGrabResult>
#include <QImage>

#include <rep_tracksegment_source.h>
#include <rep_tracksegment_replica.h>

#include "defs.h"
#include "geo.h"

class TrackSegment : public TrackSegmentSimpleSource {
    Q_OBJECT

public:
    explicit TrackSegment(QCoreApplication *parent = {}, quint32 startLap = {}, qreal mile = defs::nothing);
    static void registerMethods() {}
    void startHeartbeat() {}

    Q_INVOKABLE static QGeoCoordinate coord(QGeoPositionInfo const &positionInfo) {
        return positionInfo.coordinate();
    }
    Q_INVOKABLE static QDateTime timestamp(QGeoPositionInfo const &positionInfo) {
        return positionInfo.timestamp();
    }

public slots:
    void start  () override;
    void lap    () override;
    void pause  () override;
    void unpause() override;
    void undef  () override;

    void useUnits(qreal mile, qreal yard) override;

    void update(QGeoPositionInfo const &where) override;

protected:
    using Super = TrackSegmentSimpleSource;

    inline auto coord() const { return coord(where()); }
    inline auto when() const { return geo::timestamp(where()); }
    inline auto spd() const { return geo::speed(where()); }

    virtual void trim() {}
    void endLap();
    void updateTime(quint64 now);
    void pressRec();
    void resetStats();
    void pointCuboid(qreal lat, qreal lon, qreal ele);
    void stampTrack();

    using TrackSegmentSimpleSource::fix;

    QGeoPositionInfo lastKnownWhereabouts;
    mutable qreal normal{-90};
    bool mute{};
    qint64 lastUpdated{}, lastSampled{};
    static constexpr auto nothing = defs::nothing;
    QGeoCoordinate filteredCoord;

    qreal azimuth;
    qreal mile, nextMilestone;
    qint64 lastTime, lastSecond;
    std::deque<qint64> lapEdges;
    const quint32 firstLap{};
};

class FilteredTrack: public TrackSegment {
    Q_OBJECT

public:
    using TrackSegment::TrackSegment;

private:
    void trim() override;
    bool ripened() const;
};

#endif
